
import os, pickle, numpy as np, skimage.io
import tifffile
class MOSAICImage:
    def  __init__(self, folder_name, dualchannel=True, cam=None, iter=0, stacks_of_CamA_in_iter=None, stacks_of_CamB_in_iter=None):
        """converts MOSAIC data in a folder into MOSAICImage class"""
        
        #Attributes of a MOSAICImage
        data_folder: str #path to folder where images are
        dualchannel: bool #True by default, or False when one camera acquisition
        cam: str #name of camera used
        Cams: List #Cams is a list of lists Cams[0] is a list of all CamA tifs and Cams[1] is the same but for CamB
        shape: np.array #np.array([self.N_channel, self.N_stack, self.nZ, self.nY, self.nX])
        N_stacks: int #number of stacks acquired
        N_channels: int #number of channels acquired
        is_z: bool #True if Z stacks acquired
        nZ: int #number of Z slices
        nY: int #amount of Y pixels
        nX: int #amount of X pixels

        #these attributes are used if data is generated with scripted loops and the number of stacks per camera are unequal
        iterations: int #if using a script this is the number of total loops gully completed
        CamA_stacks_per_iter: int #how many images of CamA taken during a scripted loop
        CamB_stacks_per_iter: int #how many images of CamB taken during a scripted loop

        #initalize parameters
        self.data_folder = folder_name
        self.dualchannel = dualchannel
        self.cam = cam
        self.iterations = int(iter) # number of iterations
        self.CamA_stacks_per_iter = stacks_of_CamA_in_iter
        self.CamB_stacks_per_iter = stacks_of_CamB_in_iter
        self.tiffs_to_cams()
        self.N_channel = len(self.Cams)
        self.find_shape()

    def tiffs_to_cams(self) -> None:
        """takes all tiffs and converts to ordered lists stored in self.Cams"""
        F = os.listdir(self.data_folder)
        TIFFS = [x for x in F if ('.tif' in x and '.csv' not in x)]
        if self.dualchannel==True and self.iterations == 0: #if two channel and no scripted iterations
            self.N_stack = len(TIFFS)//2
        elif self.dualchannel==True and self.iterations >0: #if two channel and has iterations
            if self.CamA_stacks_per_iter >= self.CamB_stacks_per_iter:
                self.N_stack = self.CamA_stacks_per_iter * self.iterations
            else:
                self.N_stack = self.CamB_stacks_per_iter * self.iterations
        else: #if one channel
            self.N_stack = len(TIFFS)
        self.Cams = []
        if self.dualchannel==True and self.iterations == 0: #if two channel and no scripted iterations
            self.Cams.append([x for x in TIFFS if 'CamA' in x])
            self.Cams.append([x for x in TIFFS if 'CamB' in x])
        elif self.dualchannel==True and self.iterations >0:  #if two channel and has iterations
            if self.CamA_stacks_per_iter >= self.CamB_stacks_per_iter:
                self.Cams.append([x for x in TIFFS if 'CamA' in x])
                CamBList =[]
                for i in TIFFS:
                    if 'CamB' in i:
                        for j in range(self.CamA_stacks_per_iter):
                            CamBList.append(i)
                self.Cams.append(CamBList)  
            if self.CamB_stacks_per_iter >= self.CamA_stacks_per_iter:
                CamAList =[]
                for i in TIFFS:
                    if 'CamA' in i:
                        for j in range(self.CamB_stacks_per_iter):
                           CamAList.append(i)
                self.Cams.append(CamAList)
                self.Cams.append([x for x in TIFFS if 'CamB' in x])
        else:
            self.Cams.append([x for x in TIFFS])
        self.Cams=np.sort(self.Cams) #sorts all tiffs in order (needed for working on MacOS)

    def find_shape(self) -> None:
        """determines value of self.shape"""
        test_file = self.Cams[0][0]

        ti = self.read(test_file)
        S  = ti.shape
        del ti
        self.is_z = len(S) == 3

        if self.is_z:
            self.nZ = S[0]
        else:
            self.nZ = 1
        if self.is_z:
            self.nY = S[1]
        else:
            self.nY = S[0]
        if self.is_z:
            self.nX = S[2]
        else:
            self.nX = S[1]
        self.shape = np.array([self.N_channel, self.N_stack, self.nZ, self.nY, self.nX])
    
    def read(self, filename):
        """uses imread to read a single tif in data folder"""
        return tifffile.imread(f"{self.data_folder}/{filename}")

    def open_inx(self, ch, t):
        """opens a single stack of channel (ch) and time (t)"""
        assert(t < self.N_stack)
        assert(ch < self.N_channel)
        return self.read(self.Cams[ch][t])

    def save_inx(self, filename, ch, t, img):
        """saves all MOSAICImage to tif format"""
        CAMS = ['CamA', 'CamB']
        skimage.io.imsave(f"{filename}_CH{ch}_{CAMS[ch]}_T{str(t).rjust(5, '0')}.tif", img)

    def save_MOSAIC(self, filename):
        """uses pickle dump to save MOSAICImage"""
        f = open(filename, 'wb')
        pickle.dump(self, f)
        f.close()

def open_mosaic_image(filename):
    """opens file with pickle load"""
    with open(filename, 'rb') as f:
        obj = pickle.load(f)
    return obj