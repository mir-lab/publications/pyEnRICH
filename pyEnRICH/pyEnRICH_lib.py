"""This script contains all necessary functions to use pyEnRICH."""

#Import dependencies
import numpy as np, pandas as pd
import read_mosaic as MOSAIC
import skimage.measure, skimage.filters, skimage.morphology, skimage.io
from magicgui import magic_factory
from napari.types import ImageData, LabelsData, PointsData
import skimage.feature
import scipy.ndimage
import os
from multiprocessing import Pool
import warnings
from scipy.optimize import curve_fit, OptimizeWarning
import random
import glob
import matplotlib.pyplot as plt
import ast
import math
import seaborn as sns
import scipy.stats
import napari
import tifffile

def get_t_interval(IMG): # from Ella 
    """Returns average interval between frames of a MOSAIC image in seconds"""
    import re
    gaps = np.zeros(IMG.shape[1]-1)
    for i in range(1,IMG.shape[1]):
        try:
            gaps[i-1] = (int(re.search('%s(.*)%s' % ('msec_', 'msecAbs_'), IMG.Cams[0][i]).group(1))-int(re.search('%s(.*)%s' % ('msec_', 'msecAbs_'), IMG.Cams[0][i-1]).group(1)))/1000
        except: gaps[i-1] = np.nan
    return np.mean(gaps)

#function for 1D gaussian shape
def gauss(x, H, A, x0, sigma):
    return H + A * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))

def remove_large_labels(label_matrix, max_size=50000):
    """
    Remove labeled regions larger than max_size from the label matrix.
    
    Parameters:
    - label_matrix: The input label matrix
    - max_size: The maximum allowable region size (default is 50000 pixels)
    
    Returns:
    - The label matrix with large labeled regions removed.
    """
    # Calculate the size of each label
    label_sizes = np.bincount(label_matrix.ravel())
    # Identify labels that are too large
    large_labels = np.where(label_sizes > max_size)[0]
    # Create a mask for these large labels
    large_labels_mask = np.isin(label_matrix, large_labels)
    # Set the labels of large regions to 0 (background)
    label_matrix[large_labels_mask] = 0
    return label_matrix

def remove_small_labels(label_matrix, min_size=1000):
    """
    Remove labeled regions smaller than min_size from the label matrix.
    
    Parameters:
    - label_matrix: The input label matrix
    - min_size: The minimum allowable region size (default is 1000 pixels)
    
    Returns:
    - The label matrix with small labeled regions removed.
    """
    # Calculate the size of each label
    label_sizes = np.bincount(label_matrix.ravel())
    # Identify labels that are too large
    small_labels = np.where(label_sizes < min_size)[0]
    # Create a mask for these large labels
    small_labels_mask = np.isin(label_matrix, small_labels)
    # Set the labels of large regions to 0 (background)
    label_matrix[small_labels_mask] = 0
    return label_matrix

#function to fit to a 1D gaussian
def gauss_fit(x, y):
    mean = sum(x * y) / sum(y)
    sigma = np.sqrt(sum(y * (x - mean) ** 2) / sum(y))
    popt, pcov = curve_fit(gauss, x, y, p0=[min(y), max(y), mean, sigma])
    return popt, pcov
    
#obtain median intensity in region props function
def intensity_median(regionmask, intensity_image):
    return np.median(intensity_image[regionmask])

#obtain line profile from a set of points
def line_profile(image, point_3D, axis = "x", line_size = 1):
    if axis == "x":
        start_point = [int(point_3D[1]), (int(point_3D[2])-((line_size-1)/2))]
        end_point = [int(point_3D[1]), (int(point_3D[2]+((line_size-1)/2)))]
        line = np.asarray(skimage.measure.profile_line(image[int(point_3D[0]),:,:], start_point, end_point, linewidth=1))
        return line 
    elif axis == "y":
        start_point = [int(point_3D[1]-((line_size-1)/2)), (int(point_3D[2]))]
        end_point = [int(point_3D[1]+((line_size-1)/2)), (int(point_3D[2]))]
        line = np.asarray(skimage.measure.profile_line(image[int(point_3D[0]),:,:], start_point, end_point, linewidth=1))
        return line 
    elif axis == "z":
        start_point = [int(point_3D[0]-((line_size-1)/10)), (int(point_3D[1]))]
        end_point = [int(point_3D[0]+((line_size-1)/10)), (int(point_3D[1]))]
        line = np.asarray(skimage.measure.profile_line(image[:,:,int(point_3D[2])], start_point, end_point, linewidth=1))
        return line
    
#function to obtain a radial profile
def radial_profile(data):
    IMGshape = data.shape
    y, x = np.indices(IMGshape)
    center = data.shape[1]/2
    r = np.sqrt((x - center)**2 + (y - center)**2)
    r = r.astype(int)
    tbin = np.bincount(r.ravel(), data.ravel())
    nr = np.bincount(r.ravel())
    radialprofile = tbin / nr
    return radialprofile 

def binned_radial_profile(data, center):
    y,x = np.indices((data.shape)) # first determine radii of all pixels
    r = np.sqrt((x-center[0])**2+(y-center[1])**2)
    ind = np.argsort(r.flat) # get sorted indices
    sr = r.flat[ind] # sorted radii
    sim = data.flat[ind] # image values sorted by radii
    ri = sr.astype(np.int32) # integer part of radii (bin size = 1)
    # determining distance between changes
    deltar = ri[1:] - ri[:-1] # assume all radii represented
    rind = np.where(deltar)[0] # location of changed radius
    nr = rind[1:] - rind[:-1] # number in radius bin
    csim = np.cumsum(sim, dtype=np.float64) # cumulative sum to figure out sums for each radii bin
    tbin = csim[rind[1:]] - csim[rind[:-1]] # sum for image values in radius bins
    radialprofile = tbin/nr # the answer
    return radialprofile

#function to normalize an array
def normalize(arr, norm):
    if norm == 0:
        norm = 0.00001
    norm_arr = []
    for i in arr:
        temp = i/norm
        norm_arr.append(temp)
    return norm_arr

#calculate euclidean distances
def calculate_distance(point1, point2):
    z1, y1, x1 = point1
    z2, y2, x2 = point2

    distance = math.sqrt((z2 - z1)**2 + (y2 - y1)**2 + (x2 - x1)**2)
    return distance

#take mean value of upper quartile of an array
def get_upper_quartile_mean(arr):
    upper_quartile = np.percentile(arr, 25)
    upper_quartile_values = arr[arr >= upper_quartile]
    mean = np.mean(upper_quartile_values)
    return mean

#take mean value of lower quartile of an array
def get_lower_quartile_mean(arr):
    lower_quartile = np.percentile(arr, 25)
    lower_quartile_values = arr[arr <= lower_quartile]
    mean = np.mean(lower_quartile_values)
    return mean

# crop into 2.1um windows function, save window, return radial profiles
def crop_3d(label, sample_id, MCP_image, TF_image, initial_point, final_point, output, t, MCP_channel = 1, TF_channel = 0, normalize = None):
    #first define a 3 z stack volume and then take a max projection to gain a final cropped window

    cropped_volume_mcp = MCP_image[initial_point[0], initial_point[1]:final_point[1], initial_point[2]:final_point[2]]
    cropped_volume_tf = TF_image[initial_point[0], initial_point[1]:final_point[1], initial_point[2]:final_point[2]]
    
    if normalize == None:
        cropped_max_mcp = cropped_volume_mcp
        cropped_max_tf = cropped_volume_tf
    else:
        cropped_max_mcp = cropped_volume_mcp
        cropped_max_tf = cropped_volume_tf
        norm_cropped_max_mcp = cropped_volume_mcp/normalize[1]
        norm_cropped_max_tf = cropped_volume_tf/normalize[0]

    #calculate radial profile and mean 
    mcpRadial = radial_profile(cropped_max_mcp)
    tfRadial = radial_profile(cropped_max_tf)
    norm_mcpRadial = radial_profile(norm_cropped_max_mcp)
    norm_tfRadial = radial_profile(norm_cropped_max_tf)

    #now save cropped images
    os.makedirs(output, exist_ok=True)

    CAMS = ['CamA', 'CamB']
    skimage.io.imsave(f"{output}/{sample_id}_SPOT_mcp_CH{MCP_channel}_{CAMS[MCP_channel]}_label{label}_T{str(t).rjust(5, '0')}.tif", cropped_max_mcp/normalize[1])
    skimage.io.imsave(f"{output}/{sample_id}_SPOT_tf_CH{TF_channel}_{CAMS[TF_channel]}_label{label}_T{str(t).rjust(5, '0')}.tif", cropped_max_tf/normalize[0])

    del cropped_max_tf, cropped_max_mcp, cropped_volume_mcp, cropped_volume_tf
    if normalize != None:
        return mcpRadial, tfRadial, norm_mcpRadial, norm_tfRadial
    else:
        return mcpRadial, tfRadial

#to filter a set of labels by if they overlap with a local peak 
def filter_labels_by_seeds(label_matrix, seeds):
    # List to store labels that contain a seed point
    valid_labels = []
    for seed in seeds:
        # Find the label of the region containing the seed point
        label_value = label_matrix[tuple(seed)]
        # If the label is not 0 (background) and not already added to valid_labels, add it
        if label_value != 0 and label_value not in valid_labels:
            valid_labels.append(label_value)
    # Create a mask where only the valid labels are set to True
    mask = np.isin(label_matrix, valid_labels)
    # Use the mask to filter the label matrix
    filtered_label_matrix = np.where(mask, label_matrix, 0)
    return filtered_label_matrix

#to recolor labels to match that of the nuclear mask for later referencing (extend with pyHUBS?)
def recolor_based_on_overlap(label_matrix_original, label_matrix_reference):
    # Make a copy to avoid modifying the original
    recolored_matrix = label_matrix_original.copy()
    # Get unique labels in the reference label matrix
    unique_labels_ref = np.unique(label_matrix_reference)
    for label in unique_labels_ref:
        if label == 0:  # skip background
            continue
        # Mask where current label in reference matrix is present
        mask_ref = label_matrix_reference == label
        # Find overlapping labels in the original matrix
        overlapping_labels = np.unique(label_matrix_original[mask_ref])
        # Reassign overlapping labels in the original matrix to the current label from the reference matrix
        for overlap_label in overlapping_labels:
            if overlap_label == 0:  # skip background
                continue
            recolored_matrix[label_matrix_original == overlap_label] = label
    return recolored_matrix

#check if Ms2 spot is within nuclear mask bounds
def is_within_object_square(label_matrix, point, size):
    # Ensure the size is odd for centering around the point
    if size % 2 == 0:
        raise ValueError("Window_crop must be an odd number for centering.")
    # Extract the z-slice containing the point
    z_slice = label_matrix[point[0]]
    # Get the label value at the given point
    label_value = z_slice[point[1], point[2]]
    # If the label is background (0), return False
    if label_value == 0:
        return False
    # Define a bounding box around the point based on half the size
    half_size = size // 2
    y_start, y_end = max(point[1] - half_size, 0), min(point[1] + half_size + 1, z_slice.shape[0])
    x_start, x_end = max(point[2] - half_size, 0), min(point[2] + half_size + 1, z_slice.shape[1])
    # Extract the subregion from the slice
    subregion = z_slice[y_start:y_end, x_start:x_end]
    # Check if all pixels in the square region have the same label value
    return np.all(subregion == label_value)

#find randoms nuclear spots given a point 
def find_random_nuclear_spots(IMG, I_MCP, M, p, window_crop, ratio_nc, nuc_threshold, masks_provided=False):
    RS_points = []
    # Check if nuclear and generate a random spot (RS) near that spot
    is_RS_nuclear = True
    RS_counter = 0 
    # Random rotational sweep
    thetalist = list(range(0, 360, 30))
    sampled_list = random.sample(thetalist, 11)
    # Rotational sweep for random sites away from the MS2 spot
    for theta in sampled_list:
        RS = [int(p['Zi_MS2']), int(p['Yi_MS2'] + ((window_crop-1)*math.sin(math.radians(theta)))), int(p['Xi_MS2'] + ((window_crop-1)*math.cos(math.radians(theta))))]
        RSi = [int(RS[0]), int(RS[1]-((window_crop-1)/2)), int(RS[2]-((window_crop-1)/2))]
        RSf = [int(RS[0]), int(RS[1]+((window_crop-1)/2))+1, int(RS[2]+((window_crop-1)/2))+1]
        if RSi[2]-1 < 0 or RSf[2]+1 > (IMG.shape[4]-1) or RSi[1]-1 < 0 or RSf[1]+1 > (IMG.shape[3]-1) or RSi[0]-1 < 0 or RSf[0]+1 > (IMG.shape[2]-1):
            continue
        # Test the generated point
        is_RS_nuclear = True
        if masks_provided == False:
            for theta1 in range(0, 180, 10):
                xi = int(RS[2] + ((window_crop-1)*0.5*math.cos(math.radians(theta1))))
                yi = int(RS[1] + ((window_crop-1)*0.5*math.sin(math.radians(theta1))))
                xf = int(RS[2] - ((window_crop-1)*0.5*math.cos(math.radians(theta1))))
                yf = int(RS[1] - ((window_crop-1)*0.5*math.sin(math.radians(theta1))))
                RS_line_profile = np.asarray(skimage.measure.profile_line(I_MCP[int(RS[0]),:,:], [yi, xi], [yf, xf], linewidth=2))
                ratio = (np.median(RS_line_profile[:int((window_crop-1)/5)])/np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]))
                if (ratio > (1 + ratio_nc)) or (ratio < (1 - ratio_nc)):
                    is_RS_nuclear=False
                if (np.median(RS_line_profile[:int((window_crop-1)/5)]) >= (0.95*nuc_threshold)):
                    is_RS_nuclear=False
                elif (np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]) >= (0.95*nuc_threshold)):
                    is_RS_nuclear=False
        else: 
            is_RS_nuclear = is_within_object_square(M, RS, size=window_crop)
        # If the generated point meets criteria, find another point in that direction
        if is_RS_nuclear == True:
            RS_points.append([p['Label'], p['T'], RS[0], RS[1], RS[2]])
            RS_counter = RS_counter + 1
            if RS_counter == 3:
                break
            else:
                RS = [int(p['Zi_MS2']), int(p['Yi_MS2'] + ((window_crop-1)*1.5*math.sin(math.radians(theta)))), int(p['Xi_MS2'] + ((window_crop-1)*1.5*math.cos(math.radians(theta))))]
                RSi = [int(RS[0]), int(RS[1]-((window_crop-1)/2)), int(RS[2]-((window_crop-1)/2))]
                RSf = [int(RS[0]), int(RS[1]+((window_crop-1)/2))+1, int(RS[2]+((window_crop-1)/2))+1]
                
                if RSi[2]-1 < 0 or RSf[2]+1 > (IMG.shape[4]-1) or RSi[1]-1 < 0 or RSf[1]+1 > (IMG.shape[3]-1) or RSi[0]-1 < 0 or RSf[0]+1 > (IMG.shape[2]-1):
                    continue
                # Test the newly generated point
                is_RS_nuclear = True
                if masks_provided == False:
                    for theta2 in range(0, 180, 10):
                        xi = int(RS[2] + ((window_crop-1)*0.5*math.cos(math.radians(theta2))))
                        yi = int(RS[1] + ((window_crop-1)*0.5*math.sin(math.radians(theta2))))
                        xf = int(RS[2] - ((window_crop-1)*0.5*math.cos(math.radians(theta2))))
                        yf = int(RS[1] - ((window_crop-1)*0.5*math.sin(math.radians(theta2))))
                        RS_line_profile = np.asarray(skimage.measure.profile_line(I_MCP[int(RS[0]),:,:], [yi, xi], [yf, xf], linewidth=2))
                        ratio = (np.median(RS_line_profile[:int((window_crop-1)/5)])/np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]))
                        if (ratio > (1 + ratio_nc)) or (ratio < (1 - ratio_nc)):
                            is_RS_nuclear=False
                        if (np.median(RS_line_profile[:int((window_crop-1)/5)]) >= (0.95*nuc_threshold)):
                            is_RS_nuclear=False
                        elif (np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]) >= (0.95*nuc_threshold)):
                            is_RS_nuclear=False
                else: 
                    is_RS_nuclear = is_within_object_square(M, RS, size=window_crop)
                
                if is_RS_nuclear == True:
                    RS_points.append([p['Label'], p['T'], RS[0], RS[1], RS[2]])
                    RS_counter = RS_counter + 1
                    if RS_counter == 3:
                        break
    return RS_points

def find_random_nuclear_spots_for_widget(I_MCP, M, p, window_crop, ratio_nc, nuc_threshold, masks_provided=False):
    RS_points = []
    # Check if nuclear and generate a random spot (RS) near that spot
    is_RS_nuclear = True
    RS_counter = 0 
    # Random rotational sweep
    thetalist = list(range(0, 360, 30))
    sampled_list = random.sample(thetalist, 11)
    # Rotational sweep for random sites away from the MS2 spot
    for theta in sampled_list:
        RS = [int(p['Zi_MS2']), int(p['Yi_MS2'] + ((window_crop-1)*math.sin(math.radians(theta)))), int(p['Xi_MS2'] + ((window_crop-1)*math.cos(math.radians(theta))))]
        RSi = [int(RS[0]), int(RS[1]-((window_crop-1)/2)), int(RS[2]-((window_crop-1)/2))]
        RSf = [int(RS[0]), int(RS[1]+((window_crop-1)/2))+1, int(RS[2]+((window_crop-1)/2))+1]
        if RSi[2]-1 < 0 or RSf[2]+1 > (I_MCP.shape[2]-1) or RSi[1]-1 < 0 or RSf[1]+1 > (I_MCP.shape[1]-1) or RSi[0]-1 < 0 or RSf[0]+1 > (I_MCP.shape[0]-1):
            continue
        # Test the generated point
        is_RS_nuclear = True
        if masks_provided == False:
            for theta1 in range(0, 180, 10):
                xi = int(RS[2] + ((window_crop-1)*0.5*math.cos(math.radians(theta1))))
                yi = int(RS[1] + ((window_crop-1)*0.5*math.sin(math.radians(theta1))))
                xf = int(RS[2] - ((window_crop-1)*0.5*math.cos(math.radians(theta1))))
                yf = int(RS[1] - ((window_crop-1)*0.5*math.sin(math.radians(theta1))))
                RS_line_profile = np.asarray(skimage.measure.profile_line(I_MCP[int(RS[0]),:,:], [yi, xi], [yf, xf], linewidth=2))
                ratio = (np.median(RS_line_profile[:int((window_crop-1)/5)])/np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]))
                if (ratio > (1 + ratio_nc)) or (ratio < (1 - ratio_nc)):
                    is_RS_nuclear=False
                if (np.median(RS_line_profile[:int((window_crop-1)/5)]) >= (0.95*nuc_threshold)):
                    is_RS_nuclear=False
                elif (np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]) >= (0.95*nuc_threshold)):
                    is_RS_nuclear=False
        else: 
            is_RS_nuclear = is_within_object_square(M, RS, size=window_crop)
        # If the generated point meets criteria, find another point in that direction
        if is_RS_nuclear == True:
            RS_points.append([p['Label'], p['T'], RS[0], RS[1], RS[2]])
            RS_counter = RS_counter + 1
            if RS_counter == 3:
                break
            else:
                RS = [int(p['Zi_MS2']), int(p['Yi_MS2'] + ((window_crop-1)*1.5*math.sin(math.radians(theta)))), int(p['Xi_MS2'] + ((window_crop-1)*1.5*math.cos(math.radians(theta))))]
                RSi = [int(RS[0]), int(RS[1]-((window_crop-1)/2)), int(RS[2]-((window_crop-1)/2))]
                RSf = [int(RS[0]), int(RS[1]+((window_crop-1)/2))+1, int(RS[2]+((window_crop-1)/2))+1]
                if RSi[2]-1 < 0 or RSf[2]+1 > (I_MCP.shape[2]-1) or RSi[1]-1 < 0 or RSf[1]+1 > (I_MCP.shape[1]-1) or RSi[0]-1 < 0 or RSf[0]+1 > (I_MCP.shape[0]-1):
                    continue
                # Test the newly generated point
                is_RS_nuclear = True
                if masks_provided == False:
                    for theta2 in range(0, 180, 10):
                        xi = int(RS[2] + ((window_crop-1)*0.5*math.cos(math.radians(theta2))))
                        yi = int(RS[1] + ((window_crop-1)*0.5*math.sin(math.radians(theta2))))
                        xf = int(RS[2] - ((window_crop-1)*0.5*math.cos(math.radians(theta2))))
                        yf = int(RS[1] - ((window_crop-1)*0.5*math.sin(math.radians(theta2))))
                        RS_line_profile = np.asarray(skimage.measure.profile_line(I_MCP[int(RS[0]),:,:], [yi, xi], [yf, xf], linewidth=2))
                        ratio = (np.median(RS_line_profile[:int((window_crop-1)/5)])/np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]))
                        if (ratio > (1 + ratio_nc)) or (ratio < (1 - ratio_nc)):
                            is_RS_nuclear=False
                        if (np.median(RS_line_profile[:int((window_crop-1)/5)]) >= (0.95*nuc_threshold)):
                            is_RS_nuclear=False
                        elif (np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]) >= (0.95*nuc_threshold)):
                            is_RS_nuclear=False
                else: 
                    is_RS_nuclear = is_within_object_square(M, RS, size=window_crop)
                
                if is_RS_nuclear == True:
                    RS_points.append([p['Label'], p['T'], RS[0], RS[1], RS[2]])
                    RS_counter = RS_counter + 1
                    if RS_counter == 3:
                        break
    return RS_points

#for ONE timepoint: segments MS2 spots, finds random sites, optional edge selection, saves segmentation and csv of each spot, RS, and edge
def spot_segmentation_and_crop(t, 
    IMG, 
    DIMG,
    MIMG, 
    sample_id, 
    nuclearcycle,
    input_dir,
    output_dir,
    sigma_high = 6,
    sigma_low = 1.5,
    nuclear_point = None,
    cytoplasmic_point = None,
    thresh_value = 99,
    adjustment_factor = 1,
    TF_CHANNEL = 0,
    MCP_CHANNEL = 1, 
    window_crop = 21,
    Z_Bounds = None,
    save_edges = False,
    masks_provided = False):

    #find what nuclear cycle we are in
    if not isinstance(nuclearcycle, str):
        nc = ''
        for i in range(len(nuclearcycle)):
            if nuclearcycle[i][1][0] <= t < nuclearcycle[i][1][1]:
                nc = nuclearcycle[i][0]
        nuclearcycle = nc
    filename = f"{sample_id}"
    sample_id = f"{sample_id}_{nuclearcycle}"

    #track progression of function
    print(f'Processing {sample_id} Image. Timepoint = {t+1}')

    #create output directories for later
    os.makedirs(output_dir, exist_ok=True)
    output_dir_csv = f"{output_dir}/{sample_id}_csvs"
    os.makedirs(output_dir_csv, exist_ok=True)
    output_dir_crops = f"{output_dir}/{sample_id}_crops"
    os.makedirs(output_dir_crops, exist_ok=True)
    out_path = f"{output_dir_crops}/transcription_sites"
    os.makedirs(out_path, exist_ok=True)
    out_path = f"{output_dir_crops}/random_sites"
    os.makedirs(out_path, exist_ok=True)
    if save_edges == True:
        out_path = f"{output_dir_crops}/edge_sites"
        os.makedirs(out_path, exist_ok=True)
        edges = pd.DataFrame()

    ##Open and Initialize all the Raw, Decon, and Mask Images
    if Z_Bounds is None:
        #Open and Crop Image if unequivalent Z stacks present
        if IMG.shape[2] == DIMG.shape[2]:
            I_MCP = IMG.open_inx(MCP_CHANNEL, t)[:,:,:] 
            I_TF = IMG.open_inx(TF_CHANNEL, t)[:,:,:]
            D_MCP = DIMG.open_inx(MCP_CHANNEL, t)[:,:,:] 
            D_TF = DIMG.open_inx(TF_CHANNEL, t)[:,:,:]
        else: 
            raise ValueError("Raw and Decon Z-stack # is NOT Equivalent. Please use Z_Bounds and Try Again.")
    else: 
        I_MCP = IMG.open_inx(MCP_CHANNEL, t)[Z_Bounds[0]:Z_Bounds[1],:,:] 
        I_TF = IMG.open_inx(TF_CHANNEL, t)[Z_Bounds[0]:Z_Bounds[1],:,:]
        D_MCP = DIMG.open_inx(MCP_CHANNEL, t)[:,:,:] 
        D_TF = DIMG.open_inx(TF_CHANNEL, t)[:,:,:]

    if masks_provided == True: #open up all the nuclear masks that are provided 
        if Z_Bounds is None:
            M = MIMG.open_inx(0, t)[:,:,:]
        else:
            M = MIMG.open_inx(0, t)[Z_Bounds[0]:Z_Bounds[1],:,:]
        # M = MIMG.open_inx(0, t)[:,:,:] #only one channel for Masks

    #intiailize some key dataframes needed for later analysis
    points = pd.DataFrame()
    RS_points = [] #make empty array to hold random points
    RS_points_2 = [] #make empty array to hold random points

    ##Segment MS2 Spots
    if masks_provided == False: 
        A = skimage.filters.gaussian(I_MCP, sigma_low) - skimage.filters.gaussian(I_MCP, sigma_high) #difference of gaussians to filter out MS2 spots
        A = A > np.percentile(A, thresh_value) #threshold for MS2 spots
        A = skimage.morphology.remove_small_objects(A, min_size=6) #remove noise
        A = skimage.measure.label(A) #label
        max_label_value = np.max(A) # assess how many spots in a given frame

        #Validate Nuclear Localization -> needed if no nuclear masks are provided 
        #find the nuclear background and the cytoplasmic background levels to better assess nuclear localization
        #to find the avergae take a X and Y line profile through the two point and then take the average to help remove noise thru the points
        NI = DIMG.open_inx(MCP_CHANNEL, nuclear_point[0])[:,:,:] 
        CI = DIMG.open_inx(MCP_CHANNEL, cytoplasmic_point[0])[:,:,:] 
        nuc_background  = get_lower_quartile_mean(NI[int(nuclear_point[1]), int(nuclear_point[2]-((window_crop-1)/2)):int(nuclear_point[2]+((window_crop-1)/2))+1,  int(nuclear_point[3]-((window_crop-1)/2)):int(nuclear_point[3]+((window_crop-1)/2))+1])
        cyto_background  = get_upper_quartile_mean(CI[int(cytoplasmic_point[1]), int(cytoplasmic_point[2]-((window_crop-1)/2)):int(cytoplasmic_point[2]+((window_crop-1)/2))+1,  int(cytoplasmic_point[3]-((window_crop-1)/2)):int(cytoplasmic_point[3]+((window_crop-1)/2))+1])
        ratio_nc = 1 - (nuc_background/cyto_background)
        nuc_threshold = nuc_background*(1+ratio_nc)*adjustment_factor #anything above this value it cytoplasmic, under is nuclear      

    else: #find MS2 spots within the nucleus 
        ##Segment MS2 Spots -> rough since we use lower threshold and then adjust to local peak calling 
        A = skimage.filters.gaussian(D_MCP, sigma_low) - skimage.filters.gaussian(D_MCP, sigma_high) #difference of gaussians to filter out MS2 spots
        A = A > np.percentile(A, thresh_value) #threshold for MS2 spots
        A = skimage.morphology.remove_small_objects(A, min_size=6) #remove noise
        A = skimage.measure.label(A) #label
        max_label_value = np.max(A) # assess how many spots in a given frame

        B = D_MCP * skimage.morphology.binary_erosion(M>0, footprint = skimage.morphology.ball(3)) #remove non-nuclear things
        B = scipy.ndimage.median_filter(B, footprint = skimage.morphology.ball(1))  #maximum filter the entire normalized
        local_maxima = skimage.feature.peak_local_max(B, labels = M, num_peaks_per_label = 1)

        A = filter_labels_by_seeds(A, local_maxima)
        A = recolor_based_on_overlap(A, M)

        #for background subtraction
        BG_SUB = M-A

    ##Summarize Points
    regions = skimage.measure.regionprops(A, intensity_image=D_MCP) #identify properties of the raw MS2 spots in label matrix
    t_interval = get_t_interval(IMG)
    new_points = pd.DataFrame({
        'Filename':[filename for _ in regions],
        'Label':[r.label for r in regions],
        'NC':[nuclearcycle for _ in regions],
        'Frame':[t for _ in regions], 
        'T':[t*t_interval for _ in regions], 
        'X_MS2':[r.centroid[2] for r in regions], 
        'Y_MS2':[r.centroid[1] for r in regions],
        'Z_MS2':[r.centroid[0] for r in regions],
        'Xi_MS2':[np.round(r.weighted_centroid[2]) for r in regions],
        'Yi_MS2':[np.round(r.weighted_centroid[1]) for r in regions],
        'Zi_MS2':[np.round(r.weighted_centroid[0]) for r in regions],
        'Max Decon Intensity MCP':[r.max_intensity for r in regions],
        'Min Decon Intensity MCP':[r.min_intensity for r in regions],
        'Mean Decon Intensity MCP':[r.mean_intensity for r in regions],
        'Area MCP':[r.area for r in regions],
        'TF Decon Radial Profile':[[] for _ in regions],
        'MCP Decon Radial Profile':[[] for _ in regions],
        'TF Decon Radial Profile Normalized':[[] for _ in regions],
        'MCP Decon Radial Profile Normalized':[[] for _ in regions],
        'Nuclear?':[False for _ in regions],
        'Edge?':[False for _ in regions],
        'TF Decon Max Enrichment':[0 for _ in regions],
        'MCP Decon Max Enrichment':[0 for _ in regions]
    })
    # Now add some properties from the raw images
    # print(f"Shape I_MCP = {I_MCP.shape}, Shape A = {A.shape}")
    regions = skimage.measure.regionprops(A, intensity_image=I_MCP) 
    raw = pd.DataFrame({
        'Raw Max Intensity MCP':[r.max_intensity for r in regions],
        'Raw Min Intensity MCP':[r.min_intensity for r in regions],
        'Raw Nuclear Mean MCP':[r.mean_intensity for r in regions],
        'Max Raw Intensity MCP':[r.max_intensity for r in regions],
        'Min Raw Intensity MCP':[r.min_intensity for r in regions],
        'Mean Raw Intensity MCP':[r.mean_intensity for r in regions],
        'TF Raw Radial Profile':[[] for _ in regions],
        'MCP Raw Radial Profile':[[] for _ in regions],
        'TF Raw Radial Profile Normalized':[[] for _ in regions],
        'MCP Raw Radial Profile Normalized':[[] for _ in regions],
    })
    new_points = new_points.join(raw)

    if masks_provided == True:
        # Now add some properties from the raw images
        regions = skimage.measure.regionprops(M, intensity_image=D_TF) 
        df = pd.DataFrame({
            'Label':[r.label for r in regions],
            'Decon TF Nuclear Mean':[r.mean_intensity for r in regions]
        })
        new_points = new_points.merge(df, on='Label')

        regions = skimage.measure.regionprops(M, intensity_image=I_TF) 
        df = pd.DataFrame({
            'Label':[r.label for r in regions],
            'Raw TF Nuclear Mean':[r.mean_intensity for r in regions]
        })
        new_points = new_points.merge(df, on='Label')

        # Now add some properties from the raw images
        regions = skimage.measure.regionprops(BG_SUB, intensity_image=D_MCP) 
        df = pd.DataFrame({
            'Label':[r.label for r in regions],
            'Decon MCP Nuclear Mean with BG SUB':[r.mean_intensity for r in regions]
        })
        new_points = new_points.merge(df, on='Label')

        regions = skimage.measure.regionprops(BG_SUB, intensity_image=I_MCP) 
        df = pd.DataFrame({
            'Label':[r.label for r in regions],
            'Raw MCP Nuclear Mean with BG SUB':[r.mean_intensity for r in regions]
        })
        new_points = new_points.merge(df, on='Label')


    # create dataframe containing all nuclear points 
    for inx in range(len(new_points)): #for every label in matrix
        # print(f'Index = {inx} / {max_label_value}')
        ##nuclear validation
        p = new_points.iloc[inx] #for one label
        TS = [int(p['Zi_MS2']), int(p['Yi_MS2']), int(p['Xi_MS2'])]
        TSi = [int(TS[0])-1, int(TS[1]-((window_crop-1)/2)), int(TS[2]-((window_crop-1)/2))]
        TSf = [int(TS[0])+1, int(TS[1]+((window_crop-1)/2))+1, int(TS[2]+((window_crop-1)/2))+1]

        #if window crop is beyond image boundaries, set as nonnuclear and discard the point 
        if TSi[2]-1 < 0 or TSf[2]+1 > (DIMG.shape[4]-1) or TSi[1]-1 < 0 or TSf[1]+1 > (DIMG.shape[3]-1) or TSi[0]-1 < 0 or TSf[0]+1 > (DIMG.shape[2]-1):
            new_points.at[inx, 'Nuclear?'] = False
            new_points.at[inx, 'Edge?'] = False

        #otherwise assess nuclear localization
        else:
            if masks_provided == False:
                x = TS[2]
                y = TS[1]
                for theta in range(0, 180, 30):
                    xi = int(x + ((window_crop-1)*0.5*math.cos(math.radians(theta))))
                    yi = int(y + ((window_crop-1)*0.5*math.sin(math.radians(theta))))
                    xf = int(x - ((window_crop-1)*0.5*math.cos(math.radians(theta))))
                    yf = int(y - ((window_crop-1)*0.5*math.sin(math.radians(theta))))
                    RS_line_profile = np.asarray(skimage.measure.profile_line(I_MCP[int(TS[0]),:,:], [yi, xi], [yf, xf], linewidth=2))
                    ratio = (np.median(RS_line_profile[:int((window_crop-1)/5)])/np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]))
                    if (ratio > (1 + ratio_nc)) or (ratio < (1 - ratio_nc)): #if signal:noise less than desired ratio threshold then discard ##use this for finer control and specificity
                        new_points.at[inx, 'Edge?'] = True
                    elif (get_lower_quartile_mean(RS_line_profile[:int((window_crop-1)/5)]) >= nuc_threshold):
                        new_points.at[inx, 'Edge?'] = True
                    elif (get_lower_quartile_mean(RS_line_profile[int(window_crop-((window_crop-1)/5)):]) >= nuc_threshold):
                        new_points.at[inx, 'Edge?'] = True
                    else:
                        #now add adjusted center of MS2 point and standard deviation to the data frame
                        new_points.at[inx, 'Nuclear?'] = True
                if new_points.at[inx, 'Edge?'] == True:
                    new_points.at[inx, 'Nuclear?'] = False
            elif masks_provided == True:
                new_points.at[inx, 'Nuclear?'] = True
                #now check to assess edge status 
                new_points.at[inx, 'Edge?'] = not is_within_object_square(M, TS, size = window_crop)

        #IF IT IS NUCLEAR 
        if (new_points.at[inx, 'Nuclear?'] == True) and (new_points.at[inx, 'Edge?'] == False): #adjust RS criteria to include checking for either mask images or non-mask images
            if masks_provided == True:
                RS_points_result = find_random_nuclear_spots(DIMG, I_MCP, M, p, window_crop, ratio_nc=0, nuc_threshold = 0, masks_provided=masks_provided)
            else: 
                RS_points_result = find_random_nuclear_spots(DIMG, I_MCP, M, p, window_crop, ratio_nc, nuc_threshold, masks_provided=masks_provided)
            RS_counter = len(RS_points_result)
            for r in RS_points_result:
                RS_points.append(r)
            #now normalize to the average of the random sites found
            #for noramlization 
            if masks_provided == True:
                nuc_mean_mcp = p['Decon MCP Nuclear Mean with BG SUB']
                nuc_mean_tf = p['Decon TF Nuclear Mean']
            else:
                nuc_mean_mcp = 1
                nuc_mean_tf = 1

            if RS_counter == 0: 
                new_points.at[inx, 'Nuclear?'] = False
                new_points.at[inx, 'Edge?'] = False
            
            elif RS_counter == 1: 
                new_points.at[inx, 'Nuclear?'] = False
                new_points.at[inx, 'Edge?'] = True
            
            elif RS_counter == 3:
                #now with 3 random sites 
                #create max projection of 3 sites
                #first define a 3 z stack volume and then take a max projection to gain a final cropped window
                initial_point_1 = [int(RS_points[-1][2]), int(RS_points[-1][3]-((window_crop-1)/2)), int(RS_points[-1][4]-((window_crop-1)/2))]
                final_point_1 = [int(RS_points[-1][2]), int(RS_points[-1][3]+((window_crop-1)/2))+1, int(RS_points[-1][4]+((window_crop-1)/2))+1]

                initial_point_2 = [int(RS_points[-2][2]), int(RS_points[-2][3]-((window_crop-1)/2)), int(RS_points[-2][4]-((window_crop-1)/2))]
                final_point_2 = [int(RS_points[-2][2]), int(RS_points[-2][3]+((window_crop-1)/2))+1, int(RS_points[-2][4]+((window_crop-1)/2))+1]

                initial_point_3 = [int(RS_points[-3][2]), int(RS_points[-3][3]-((window_crop-1)/2)), int(RS_points[-3][4]-((window_crop-1)/2))]
                final_point_3 = [int(RS_points[-3][2]), int(RS_points[-3][3]+((window_crop-1)/2))+1, int(RS_points[-3][4]+((window_crop-1)/2))+1]
    
                cropped_volume_mcp_1 = D_MCP[initial_point_1[0], initial_point_1[1]:final_point_1[1], initial_point_1[2]:final_point_1[2]]
                cropped_max_mcp_1 = cropped_volume_mcp_1/nuc_mean_mcp
                cropped_volume_tf_1 = D_TF[initial_point_1[0], initial_point_1[1]:final_point_1[1], initial_point_1[2]:final_point_1[2]]
                cropped_max_tf_1 = cropped_volume_tf_1/nuc_mean_tf

                cropped_volume_mcp_2 = D_MCP[initial_point_2[0], initial_point_2[1]:final_point_2[1], initial_point_2[2]:final_point_2[2]]
                cropped_max_mcp_2 = cropped_volume_mcp_2/nuc_mean_mcp
                cropped_volume_tf_2 = D_TF[initial_point_2[0], initial_point_2[1]:final_point_2[1], initial_point_2[2]:final_point_2[2]]
                cropped_max_tf_2 = cropped_volume_tf_2/nuc_mean_tf

                cropped_volume_mcp_3 = D_MCP[initial_point_3[0], initial_point_3[1]:final_point_3[1], initial_point_3[2]:final_point_3[2]]
                cropped_max_mcp_3 = cropped_volume_mcp_3/nuc_mean_mcp
                cropped_volume_tf_3 = D_TF[initial_point_3[0], initial_point_3[1]:final_point_3[1], initial_point_3[2]:final_point_3[2]]
                cropped_max_tf_3 = cropped_volume_tf_3/nuc_mean_tf

                average_RP_cropped_tf = (cropped_max_tf_3 + cropped_max_tf_2 + cropped_max_tf_1)/3
                avg_tf = np.mean(average_RP_cropped_tf)
                average_RP_cropped_mcp = (cropped_max_mcp_3 + cropped_max_mcp_2 + cropped_max_mcp_1)/3
                avg_mcp = np.mean(average_RP_cropped_mcp)

                #calculate radial profile and mean 
                mcpRP = radial_profile(average_RP_cropped_mcp)
                tfRP = radial_profile(average_RP_cropped_tf)
                if masks_provided == False:
                    NtfRP = normalize(tfRP, tfRP[-1])
                    NmcpRP = normalize(mcpRP, mcpRP[-1])  
                else:
                    # NtfRP = normalize(tfRP, p['Decon TF Nuclear Mean'])
                    # NmcpRP = normalize(mcpRP, p['Decon MCP Nuclear Mean'])
                    # NtfRP = normalize(tfRP, tfRP[-1])
                    # NmcpRP = normalize(mcpRP, mcpRP[-1])
                    NtfRP = normalize(tfRP, 1)
                    NmcpRP = normalize(mcpRP, 1)
                #now save cropped images
                out_path = f"{output_dir_crops}/random_sites"
                os.makedirs(out_path, exist_ok=True)

                CAMS = ['CamA', 'CamB']
                if masks_provided == False:
                    skimage.io.imsave(f"{out_path}/{sample_id}_SPOT_mcp_CH{MCP_CHANNEL}_{CAMS[MCP_CHANNEL]}_label{p['Label']}_T{str(t).rjust(5, '0')}.tif", average_RP_cropped_mcp)
                    skimage.io.imsave(f"{out_path}/{sample_id}_SPOT_tf_CH{TF_CHANNEL}_{CAMS[TF_CHANNEL]}_label{p['Label']}_T{str(t).rjust(5, '0')}.tif", average_RP_cropped_tf)
                else:
                    skimage.io.imsave(f"{out_path}/{sample_id}_SPOT_mcp_CH{MCP_CHANNEL}_{CAMS[MCP_CHANNEL]}_label{p['Label']}_T{str(t).rjust(5, '0')}.tif", average_RP_cropped_mcp)
                    skimage.io.imsave(f"{out_path}/{sample_id}_SPOT_tf_CH{TF_CHANNEL}_{CAMS[TF_CHANNEL]}_label{p['Label']}_T{str(t).rjust(5, '0')}.tif", average_RP_cropped_tf)
                mcpRP_max = NmcpRP[0]
                tfRP_max = NtfRP[0]
                RS_points_2.append([p['NC'], p['Label'], p['T'], int(TS[2]), int(TS[1]), int(TS[0]), str(tfRP), str(mcpRP), str(NtfRP), str(NmcpRP), mcpRP_max, tfRP_max])

                # now crop a window and save radial profiles for that point
                out_path = f"{output_dir_crops}/transcription_sites"
                os.makedirs(out_path, exist_ok=True)
                if masks_provided == False:
                    # (label, sample_id, MCP_image, TF_image, initial_point, final_point, output, t, MCP_channel = 1, TF_channel = 0, normalize = None)
                    mcpRP, tfRP = crop_3d(p["Label"], sample_id, D_MCP, D_TF, TSi, TSf, out_path, t)
                else:
                    mcpRP, tfRP, norm_mcpRP, norm_tfRP = crop_3d(p["Label"], sample_id, D_MCP, D_TF, TSi, TSf, out_path, t, normalize=[p['Decon TF Nuclear Mean'], p['Decon MCP Nuclear Mean with BG SUB']])
                new_points.at[inx, 'TF Decon Radial Profile'] = str(tfRP)
                new_points.at[inx, 'MCP Decon Radial Profile'] = str(mcpRP)
                if masks_provided == False:
                    NtfRP = normalize(tfRP, tfRP[-1])
                    NmcpRP = normalize(mcpRP, mcpRP[-1])  
                else:
                    # NtfRP = normalize(tfRP, p['Decon TF Nuclear Mean'])
                    # NmcpRP = normalize(mcpRP, p['Decon MCP Nuclear Mean'])
                    # NtfRP = normalize(tfRP, tfRP[-1])
                    # NmcpRP = normalize(mcpRP, mcpRP[-1])   
                    NtfRP = normalize(norm_tfRP, 1)
                    NmcpRP = normalize(norm_mcpRP, 1)
                new_points.at[inx, 'TF Decon Radial Profile Normalized'] = str(NtfRP)
                new_points.at[inx, 'MCP Decon Radial Profile Normalized'] = str(NmcpRP)
                new_points.at[inx, 'TF Decon Max Enrichment'] = np.max(tfRP)
                new_points.at[inx, 'MCP Decon Max Enrichment'] = np.max(mcpRP)
                # print(avg_tf)
            
            elif RS_counter == 2:
                #now with 2 random sites 
                #create max projection of 2 sites
                #first define a 2 z stack volume and then take a max projection to gain a final cropped window
                initial_point_1 = [int(RS_points[-1][2]), int(RS_points[-1][3]-((window_crop-1)/2)), int(RS_points[-1][4]-((window_crop-1)/2))]
                final_point_1 = [int(RS_points[-1][2]), int(RS_points[-1][3]+((window_crop-1)/2))+1, int(RS_points[-1][4]+((window_crop-1)/2))+1]

                initial_point_2 = [int(RS_points[-2][2]), int(RS_points[-2][3]-((window_crop-1)/2)), int(RS_points[-2][4]-((window_crop-1)/2))]
                final_point_2 = [int(RS_points[-2][2]), int(RS_points[-2][3]+((window_crop-1)/2))+1, int(RS_points[-2][4]+((window_crop-1)/2))+1]
    
                cropped_volume_mcp_1 = D_MCP[initial_point_1[0], initial_point_1[1]:final_point_1[1], initial_point_1[2]:final_point_1[2]]
                cropped_max_mcp_1 = cropped_volume_mcp_1/nuc_mean_mcp
                cropped_volume_tf_1 = D_TF[initial_point_1[0], initial_point_1[1]:final_point_1[1], initial_point_1[2]:final_point_1[2]]
                cropped_max_tf_1 = cropped_volume_tf_1/nuc_mean_tf

                cropped_volume_mcp_2 = D_MCP[initial_point_2[0], initial_point_2[1]:final_point_2[1], initial_point_2[2]:final_point_2[2]]
                cropped_max_mcp_2 = cropped_volume_mcp_2/nuc_mean_mcp
                cropped_volume_tf_2 = D_TF[initial_point_2[0], initial_point_2[1]:final_point_2[1], initial_point_2[2]:final_point_2[2]]
                cropped_max_tf_2 = cropped_volume_tf_2/nuc_mean_tf

                average_RP_cropped_tf = (cropped_max_tf_2 + cropped_max_tf_1)/2
                avg_tf = np.mean(average_RP_cropped_tf)
                average_RP_cropped_mcp = (cropped_max_mcp_2 + cropped_max_mcp_1)/2
                avg_mcp = np.mean(average_RP_cropped_mcp)

                #calculate radial profile and mean 
                mcpRP = radial_profile(average_RP_cropped_mcp)
                tfRP = radial_profile(average_RP_cropped_tf)
                if masks_provided == False:
                    NtfRP = normalize(tfRP, tfRP[-1])
                    NmcpRP = normalize(mcpRP, mcpRP[-1])  
                else:
                    # NtfRP = normalize(tfRP, p['Decon TF Nuclear Mean'])
                    # NmcpRP = normalize(mcpRP, p['Decon MCP Nuclear Mean'])
                    # NtfRP = normalize(tfRP, tfRP[-1])
                    # NmcpRP = normalize(mcpRP, mcpRP[-1])  
                    NtfRP = normalize(tfRP, 1)
                    NmcpRP = normalize(mcpRP, 1)

                #now save cropped images
                out_path = f"{output_dir_crops}/random_sites"
                os.makedirs(out_path, exist_ok=True)

                CAMS = ['CamA', 'CamB']
                skimage.io.imsave(f"{out_path}/{sample_id}_SPOT_mcp_CH{MCP_CHANNEL}_{CAMS[MCP_CHANNEL]}_label{p['Label']}_T{str(t).rjust(5, '0')}.tif", average_RP_cropped_mcp)
                skimage.io.imsave(f"{out_path}/{sample_id}_SPOT_tf_CH{TF_CHANNEL}_{CAMS[TF_CHANNEL]}_label{p['Label']}_T{str(t).rjust(5, '0')}.tif", average_RP_cropped_tf)
                mcpRP_max = mcpRP[0]
                tfRP_max = tfRP[0]
                RS_points_2.append([p['NC'], p['Label'], p['T'], int(TS[2]), int(TS[1]), int(TS[0]), str(tfRP), str(mcpRP), str(NtfRP), str(NmcpRP), mcpRP_max, tfRP_max])

                # now crop a window and save radial profiles for that point
                out_path = f"{output_dir_crops}/transcription_sites"
                os.makedirs(out_path, exist_ok=True)
                if masks_provided == False:
                    mcpRP, tfRP = crop_3d(p["Label"], sample_id, D_MCP, D_TF, TSi, TSf, out_path, t)
                else:
                    mcpRP, tfRP, norm_mcpRP, norm_tfRP = crop_3d(p["Label"], sample_id, D_MCP, D_TF, TSi, TSf, out_path, t, normalize=[p['Decon TF Nuclear Mean'], p['Decon MCP Nuclear Mean with BG SUB']])
                new_points.at[inx, 'TF Decon Radial Profile'] = str(norm_tfRP)
                new_points.at[inx, 'MCP Decon Radial Profile'] = str(norm_mcpRP)
                if masks_provided == False:
                    NtfRP = normalize(tfRP, tfRP[-1])
                    NmcpRP = normalize(mcpRP, mcpRP[-1])  
                else:
                    # NtfRP = normalize(tfRP, p['Decon TF Nuclear Mean'])
                    # NmcpRP = normalize(mcpRP, p['Decon MCP Nuclear Mean'])
                    # NtfRP = normalize(tfRP, tfRP[-1])
                    # NmcpRP = normalize(mcpRP, mcpRP[-1])   
                    NtfRP = normalize(tfRP, 1)
                    NmcpRP = normalize(mcpRP, 1)
                new_points.at[inx, 'TF Decon Radial Profile Normalized'] = str(NtfRP)
                new_points.at[inx, 'MCP Decon Radial Profile Normalized'] = str(NmcpRP)
                new_points.at[inx, 'TF Decon Max Enrichment'] = np.max(tfRP)
                new_points.at[inx, 'MCP Decon Max Enrichment'] = np.max(mcpRP)
                # print(avg_tf)

        elif (new_points.at[inx, 'Edge?'] == True) and (new_points.at[inx, 'Nuclear?'] == True) and (save_edges == True):
            out_path = f"{output_dir_crops}/edge_sites"
            os.makedirs(out_path, exist_ok=True)
            
            #edge random sites
            if masks_provided == True:
                RS_points_result = find_random_nuclear_spots(DIMG, I_MCP, M, p, window_crop, ratio_nc = 0, nuc_threshold = 0, masks_provided=masks_provided)
            else: 
                RS_points_result = find_random_nuclear_spots(DIMG, I_MCP, M, p, window_crop, ratio_nc, nuc_threshold, masks_provided=masks_provided)
            RS_counter = len(RS_points_result)
            for r in RS_points_result:
                RS_points.append(r)
                        #now normalize to the average of the random sites found
            #for noramlization 
            if masks_provided == True:
                nuc_mean_mcp = p['Decon MCP Nuclear Mean with BG SUB']
                nuc_mean_tf = p['Decon TF Nuclear Mean']
            else:
                nuc_mean_mcp = 1
                nuc_mean_tf = 1
            if RS_counter == 0: 
                new_points.at[inx, 'Nuclear?'] = False
                new_points.at[inx, 'Edge?'] = False
            
            elif RS_counter == 1: 
                new_points.at[inx, 'Nuclear?'] = False
                new_points.at[inx, 'Edge?'] = False
            
            elif RS_counter == 3:
                #now with 3 random sites 
                #create max projection of 3 sites
                #first define a 3 z stack volume and then take a max projection to gain a final cropped window
                initial_point_1 = [int(RS_points[-1][2]), int(RS_points[-1][3]-((window_crop-1)/2)), int(RS_points[-1][4]-((window_crop-1)/2))]
                final_point_1 = [int(RS_points[-1][2]), int(RS_points[-1][3]+((window_crop-1)/2))+1, int(RS_points[-1][4]+((window_crop-1)/2))+1]

                initial_point_2 = [int(RS_points[-2][2]), int(RS_points[-2][3]-((window_crop-1)/2)), int(RS_points[-2][4]-((window_crop-1)/2))]
                final_point_2 = [int(RS_points[-2][2]), int(RS_points[-2][3]+((window_crop-1)/2))+1, int(RS_points[-2][4]+((window_crop-1)/2))+1]

                initial_point_3 = [int(RS_points[-3][2]), int(RS_points[-3][3]-((window_crop-1)/2)), int(RS_points[-3][4]-((window_crop-1)/2))]
                final_point_3 = [int(RS_points[-3][2]), int(RS_points[-3][3]+((window_crop-1)/2))+1, int(RS_points[-3][4]+((window_crop-1)/2))+1]
    
                cropped_volume_mcp_1 = D_MCP[initial_point_1[0], initial_point_1[1]:final_point_1[1], initial_point_1[2]:final_point_1[2]]
                cropped_max_mcp_1 = cropped_volume_mcp_1/nuc_mean_mcp
                cropped_volume_tf_1 = D_TF[initial_point_1[0], initial_point_1[1]:final_point_1[1], initial_point_1[2]:final_point_1[2]]
                cropped_max_tf_1 = cropped_volume_tf_1/nuc_mean_tf

                cropped_volume_mcp_2 = D_MCP[initial_point_2[0], initial_point_2[1]:final_point_2[1], initial_point_2[2]:final_point_2[2]]
                cropped_max_mcp_2 = cropped_volume_mcp_2/nuc_mean_mcp
                cropped_volume_tf_2 = D_TF[initial_point_2[0], initial_point_2[1]:final_point_2[1], initial_point_2[2]:final_point_2[2]]
                cropped_max_tf_2 = cropped_volume_tf_2/nuc_mean_tf

                cropped_volume_mcp_3 = D_MCP[initial_point_3[0], initial_point_3[1]:final_point_3[1], initial_point_3[2]:final_point_3[2]]
                cropped_max_mcp_3 = cropped_volume_mcp_3/nuc_mean_mcp
                cropped_volume_tf_3 = D_TF[initial_point_3[0], initial_point_3[1]:final_point_3[1], initial_point_3[2]:final_point_3[2]]
                cropped_max_tf_3 = cropped_volume_tf_3/nuc_mean_tf

                average_RP_cropped_tf = (cropped_max_tf_3 + cropped_max_tf_2 + cropped_max_tf_1)/3
                avg_tf = np.mean(average_RP_cropped_tf)
                average_RP_cropped_mcp = (cropped_max_mcp_3 + cropped_max_mcp_2 + cropped_max_mcp_1)/3
                avg_mcp = np.mean(average_RP_cropped_mcp)

                # now crop a window and save radial profiles for that point
                out_path = f"{output_dir_crops}/edge_sites"
                os.makedirs(out_path, exist_ok=True)
                if masks_provided == False:
                    mcpRP, tfRP = crop_3d(p["Label"], sample_id, D_MCP, D_TF, TSi, TSf, out_path, t)
                else:
                    mcpRP, tfRP, norm_mcpRP, norm_tfRP = crop_3d(p["Label"], sample_id, D_MCP, D_TF, TSi, TSf, out_path, t, normalize=[p['Decon TF Nuclear Mean'], p['Decon MCP Nuclear Mean with BG SUB']])
                new_points.at[inx, 'TF Decon Radial Profile'] = str(tfRP)
                new_points.at[inx, 'MCP Decon Radial Profile'] = str(mcpRP)
                if masks_provided == False:
                    NtfRP = normalize(tfRP, tfRP[-1])
                    NmcpRP = normalize(mcpRP, mcpRP[-1])  
                else:
                    # NtfRP = normalize(tfRP, p['Decon TF Nuclear Mean'])
                    # NmcpRP = normalize(mcpRP, p['Decon MCP Nuclear Mean'])
                    # NtfRP = normalize(tfRP, tfRP[-1])
                    # NmcpRP = normalize(mcpRP, mcpRP[-1])   
                    NtfRP = normalize(norm_tfRP, 1)
                    NmcpRP = normalize(norm_mcpRP, 1)
                new_points.at[inx, 'TF Decon Radial Profile Normalized'] = str(NtfRP)
                new_points.at[inx, 'MCP Decon Radial Profile Normalized'] = str(NmcpRP)
                new_points.at[inx, 'TF Decon Max Enrichment'] = np.max(tfRP)
                new_points.at[inx, 'MCP Decon Max Enrichment'] = np.max(mcpRP)
            
            elif RS_counter == 2:
                #now with 3 random sites 
                #create max projection of 3 sites
                #first define a 3 z stack volume and then take a max projection to gain a final cropped window
                initial_point_1 = [int(RS_points[-1][2]), int(RS_points[-1][3]-((window_crop-1)/2)), int(RS_points[-1][4]-((window_crop-1)/2))]
                final_point_1 = [int(RS_points[-1][2]), int(RS_points[-1][3]+((window_crop-1)/2))+1, int(RS_points[-1][4]+((window_crop-1)/2))+1]

                initial_point_2 = [int(RS_points[-2][2]), int(RS_points[-2][3]-((window_crop-1)/2)), int(RS_points[-2][4]-((window_crop-1)/2))]
                final_point_2 = [int(RS_points[-2][2]), int(RS_points[-2][3]+((window_crop-1)/2))+1, int(RS_points[-2][4]+((window_crop-1)/2))+1]
    
                cropped_volume_mcp_1 = D_MCP[initial_point_1[0], initial_point_1[1]:final_point_1[1], initial_point_1[2]:final_point_1[2]]
                cropped_max_mcp_1 = cropped_volume_mcp_1/nuc_mean_mcp
                cropped_volume_tf_1 = D_TF[initial_point_1[0], initial_point_1[1]:final_point_1[1], initial_point_1[2]:final_point_1[2]]
                cropped_max_tf_1 = cropped_volume_tf_1/nuc_mean_tf

                cropped_volume_mcp_2 = D_TF[initial_point_2[0], initial_point_2[1]:final_point_2[1], initial_point_2[2]:final_point_2[2]]
                cropped_max_mcp_2 = cropped_volume_mcp_2/nuc_mean_mcp
                cropped_volume_tf_2 = D_MCP[initial_point_2[0], initial_point_2[1]:final_point_2[1], initial_point_2[2]:final_point_2[2]]
                cropped_max_tf_2 = cropped_volume_tf_2/nuc_mean_tf

                average_RP_cropped_tf = (cropped_max_tf_2 + cropped_max_tf_1)/2
                avg_tf = np.mean(average_RP_cropped_tf)
                average_RP_cropped_mcp = (cropped_max_mcp_2 + cropped_max_mcp_1)/2
                avg_mcp = np.mean(average_RP_cropped_mcp)

                # now crop a window and save radial profiles for that point
                out_path = f"{output_dir_crops}/edge_sites"
                os.makedirs(out_path, exist_ok=True)
                if masks_provided == False:
                    mcpRP, tfRP = crop_3d(p["Label"], sample_id, D_MCP, D_TF, TSi, TSf, out_path, t)
                else:
                    mcpRP, tfRP, norm_mcpRP, norm_tfRP = crop_3d(p["Label"], sample_id, D_MCP, D_TF, TSi, TSf, out_path, t, normalize=[p['Decon TF Nuclear Mean'], p['Decon MCP Nuclear Mean with BG SUB']])
                new_points.at[inx, 'TF Decon Radial Profile'] = str(norm_tfRP)
                new_points.at[inx, 'MCP Decon Radial Profile'] = str(norm_mcpRP)
                if masks_provided == False:
                    NtfRP = normalize(tfRP, tfRP[-1])
                    NmcpRP = normalize(mcpRP, mcpRP[-1])  
                else:
                    # NtfRP = normalize(tfRP, p['Decon TF Nuclear Mean'])
                    # NmcpRP = normalize(mcpRP, p['Decon MCP Nuclear Mean'])
                    # NtfRP = normalize(tfRP, tfRP[-1])
                    # NmcpRP = normalize(mcpRP, mcpRP[-1])   
                    NtfRP = normalize(tfRP, 1)
                    NmcpRP = normalize(mcpRP, 1)
                new_points.at[inx, 'TF Decon Radial Profile Normalized'] = str(NtfRP)
                new_points.at[inx, 'MCP Decon Radial Profile Normalized'] = str(NmcpRP)
                new_points.at[inx, 'TF Decon Max Enrichment'] = np.max(tfRP)
                new_points.at[inx, 'MCP Decon Max Enrichment'] = np.max(mcpRP)
    
    #saving checkpoint!!
    #save the coordinates for the RS for downstream stuff
    RS_df = pd.DataFrame(RS_points_2, columns=['NC',
                                               'Label', 
                                               'T', 
                                               'X_MS2', 
                                               'Y_MS2', 
                                               'Z_MS2', 
                                               'TF Decon RS Radial Profile', 
                                               'MCP Decon RS Radial Profile', 
                                               'TF Decon RS Radial Profile Normalized', 
                                               'MCP Decon RS Radial Profile Normalized',
                                               'TF Decon RS Max Enrichment',
                                               'MCP Decon RS Max Enrichment'])
    out_path = f"{input_dir}/pyEnRICH_Random_Sites_CSVs_per_Timepoint"
    os.makedirs(out_path, exist_ok=True)
    RS_df.to_csv(f"{out_path}/{sample_id}_RS_Coordinates_T{str(t).rjust(5, '0')}.csv")


    #to save dropped points to show edge effects from the nucleus 
    if save_edges == True:
        # edges = edges.append(new_points[new_points['Edge?']==True], ignore_index=True)
        condition = (new_points['Nuclear?'] == True) 
        points = pd.concat([points, new_points[condition]], ignore_index=True)
        # points = pd.concat([points, new_points[new_points['Nuclear?']==True or new_points['Edge?']==True]], ignore_index=True)
    else:
        #Add NUCLEAR points to list and save as csv for each timepoint
        # points = points.append(new_points[new_points['Nuclear?']==True], ignore_index=True)
        filtered_points = new_points[(new_points['Nuclear?'] == True) & (new_points['Edge?'] == False)]
        points = pd.concat([points, filtered_points], ignore_index=True)

    out_path = f"{input_dir}/pyEnRICH_CSVs_per_Timepoint"
    os.makedirs(out_path, exist_ok=True)
    points.to_csv(f"{out_path}/{sample_id}_pyEnRICH_T{str(t).rjust(5, '0')}.csv")

    #save segmentation images
    os.makedirs(f"{input_dir}/MS2_Masks", exist_ok=True)
    IMG.save_inx(f"{input_dir}/MS2_Masks/MS2_Masks", MCP_CHANNEL, t, A)
    return

#take individ timepoint csvs and concat into one dataframe
def concatenate_time_points(input_folder, output_folder, nc):
    all_files = glob.glob(os.path.join(input_folder, "*.csv"))
    df = pd.concat((pd.read_csv(f, engine='python') for f in all_files), ignore_index=True)
    df_filtered = df[df['NC'] == nc]
    df_filtered.to_csv(output_folder)
    return df

#this takes all individual data points and cropped images across time and analyzes them 
def analyze_images(window_crop, input_dir, sample_id, set_type = "transcription_sites", MCP_channel=1, TF_channel=0):
    ##Open Directories and MOSAIC Images
    cropped_dir  = f"{input_dir}/{sample_id}_crops"
    analysis_dir  = f"{input_dir}/{sample_id}_analysis"
    os.makedirs(analysis_dir, exist_ok=True)
    IMG = MOSAIC.MOSAICImage(f"{cropped_dir}/{set_type}", dualchannel=True, cam=['CamA', 'CamB']) #array([   2, 1759,    1,   20,   20])

    #find image shape and create average placeholder
    IMGshape = IMG.shape[1:]
    AvgIMG_MCP = np.zeros(IMGshape, dtype=np.float16)
    AvgIMG_TF = np.zeros(IMGshape, dtype=np.float16)

    #loop and avg all images
    for t in range(IMGshape[0]):
        # print(f"Processing Image {t} / {IMGshape[0]}")
        if IMG.open_inx(MCP_channel,t).shape != (window_crop, window_crop):
            continue
        else:
            AvgIMG_MCP[t,:,:,:] = IMG.open_inx(MCP_channel, t)
            AvgIMG_TF[t,:,:,:] = IMG.open_inx(TF_channel, t)

    #save the resulting images
    skimage.io.imsave(f'{cropped_dir}/{sample_id}_{set_type}_Stack_MCP.tif', AvgIMG_MCP)
    skimage.io.imsave(f'{cropped_dir}/{sample_id}_{set_type}_Stack_TF.tif', AvgIMG_TF)

    #average all the added images
    AvgIMG_MCP_AVG = np.mean(AvgIMG_MCP, axis = 0)
    AvgIMG_TF_AVG = np.mean(AvgIMG_TF, axis = 0)   

    #save the resulting images
    skimage.io.imsave(f'{analysis_dir}/{sample_id}_{set_type}_Average_MCP.tif', AvgIMG_MCP_AVG)
    skimage.io.imsave(f'{analysis_dir}/{sample_id}_{set_type}_Average_TF.tif', AvgIMG_TF_AVG)

    # #median all the added images
    # AvgIMG_MCP_MED = np.median(AvgIMG_MCP, axis = 0)
    # AvgIMG_TF_MED = np.median(AvgIMG_TF, axis = 0)

    # #save the resulting images
    # skimage.io.imsave(f'{analysis_dir}/{sample_id}_{set_type}_Median_MCP.tif', AvgIMG_MCP_MED)
    # skimage.io.imsave(f'{analysis_dir}/{sample_id}_{set_type}_Median_TF.tif', AvgIMG_TF_MED)
    return

def find_average_RP(DATA, profile_type, size):
    # Filter out rows where the data is not the correct size
    valid_data = DATA[DATA[profile_type].apply(lambda x: len(x) == size)]
    
    # Convert the lists/arrays in each cell to a 2D NumPy array
    array_data = np.vstack(valid_data[profile_type].apply(np.array))
    
    # Calculate the average along the rows
    average_rp = array_data.mean(axis=0)
    #find average radial profile
                            # def find_average_RP(DATA, profile_type, size):
                            #     average = np.zeros(size) 
                            #     # print(len(average))
                            #     counter = 0
                            #     for row in range(DATA.shape[0]):
                            #         p = DATA.iloc[row][profile_type]
                            #         # print(p, "yo")
                            #         if len(p) == size:
                            #             for i in range(len(average)):
                            #                 average[i] = average[i] + p[i]
                            #         else:
                            #             counter = counter + 1
                            #             continue
                            #     for i in range(len(average)):
                            #         average[i] = average[i]/(DATA.shape[0]-counter)   
                            #     return np.array(average)
    return average_rp

def find_sem(DATA, average, profile_type, size):
    # Filter out rows where the data is not the correct size
    valid_data = DATA[DATA[profile_type].apply(lambda x: len(x) == size)]
    
    # Convert the lists/arrays in each cell to a 2D NumPy array
    array_data = np.vstack(valid_data[profile_type].apply(np.array))
    
    # Calculate the squared differences
    squared_diff = (array_data - average) ** 2
    
    # Calculate the standard error
    sem = np.sqrt(squared_diff.sum(axis=0) / (len(valid_data) * (len(valid_data) - 1))) / np.sqrt(len(valid_data))
    
    return sem

                        # #find standard error
                        # def find_sem(DATA, average, profile_type, size):
                        #     standarderror = np.zeros(size) 
                        #     counter = 0 
                        #     for row in range(DATA.shape[0]):
                        #         p = DATA.iloc[row][profile_type]
                        #         if len(p) == size:
                        #             for i in range(len(standarderror)):
                        #                 standarderror[i] = standarderror[i] + ((p[i]-average[i])*(p[i]-average[i]))
                        #         else:
                        #             counter = counter + 1
                        #             continue
                        #     for i in range(len(average)):
                        #         top = standarderror[i]/(DATA.shape[0]-counter-1)
                        #         bottom = DATA.shape[0]-counter
                        #         standarderror[i] = (math.sqrt(top))/(math.sqrt(bottom))   
                        #     return np.array(standarderror)

#this generates enrichment plots and figures
def generate_figures(input_dir, output_dir, sample_id, nuclearcycle, window_crop, save_edges = False, dark_background = True, MCP_channel = 1, TF_channel = 0):
    #what nuclear cycle??
    # if not isinstance(nuclearcycle, str):
    #     nc = ''
    #     for i in range(len(nuclearcycle)):
    #         if nuclearcycle[i][1][0] <= t < nuclearcycle[i][1][1]:
    #             nc = nuclearcycle[i][0]
    #     nuclearcycle = nc   
    filename = f"{sample_id}" 
    sample_id = f"{sample_id}_{nuclearcycle}"
    analysis_dir  = f"{output_dir}/{sample_id}_analysis"
    os.makedirs(analysis_dir, exist_ok=True)

    # to make the images
    analyze_images(window_crop, output_dir, sample_id, set_type = "transcription_sites")
    analyze_images(window_crop, output_dir, sample_id, set_type = "random_sites")

    #first we need to contenate all the timepoints to achieve time-averaged profiles


    TS_DATA = concatenate_time_points(f"{input_dir}/pyEnRICH_CSVs_per_Timepoint", f"{output_dir}/{sample_id}_csvs/{sample_id}_all_transcription_sites.csv", nuclearcycle)
    RS_DATA = concatenate_time_points(f"{input_dir}/pyEnRICH_Random_Sites_CSVs_per_Timepoint", f"{output_dir}/{sample_id}_csvs/{sample_id}_all_random_sites.csv", nuclearcycle)
    
    TS_DATA = pd.read_csv(f"{output_dir}/{sample_id}_csvs/{sample_id}_all_transcription_sites.csv")
    RS_DATA = pd.read_csv(f"{output_dir}/{sample_id}_csvs/{sample_id}_all_random_sites.csv")


    # Assuming EDGE_DATA is your DataFrame
    if save_edges:
        analyze_images(window_crop, output_dir, sample_id, set_type="edge_sites")
        EDGE_DATA = TS_DATA[TS_DATA["Edge?"] == True]
        TS_DATA = TS_DATA[TS_DATA["Edge?"] == False]
        # print(TS_DATA.shape)

        # Convert string representations of lists back to NumPy arrays
        mask = EDGE_DATA["TF Decon Radial Profile Normalized"].apply(lambda x: isinstance(x, str))
        EDGE_DATA.loc[mask, "TF Decon Radial Profile Normalized"] = EDGE_DATA.loc[mask, "TF Decon Radial Profile Normalized"].apply(
            lambda x: np.fromstring(x.strip('[]'), dtype=float, sep=', ')
        )
        # Find the size of the first non-empty radial profile in TS_DATA
        first_valid_EDGE = EDGE_DATA["TF Decon Radial Profile Normalized"].dropna().iloc[0]
        size_EDGE = len(first_valid_EDGE) if isinstance(first_valid_EDGE, (np.ndarray, list)) else 0
        # print(size_EDGE)

        # Calculate averages and standard errors for ES_DATA
        if size_EDGE > 0:
            Y_EDGE = find_average_RP(EDGE_DATA, "TF Decon Radial Profile Normalized", size_EDGE)
            SEM_EDGE = find_sem(EDGE_DATA, Y_EDGE, "TF Decon Radial Profile Normalized", size_EDGE)
        else:
            print("No valid radial profiles found in EDGE_DATA")

    # Convert string representations of lists back to NumPy arrays and normalize radial profiles within dataframes
    for df in [TS_DATA, RS_DATA]:
        profile_column = "TF Decon Radial Profile Normalized" if "TF Decon Radial Profile Normalized" in df.columns else "TF Decon RS Radial Profile Normalized"
        
        mask = df[profile_column].apply(lambda x: isinstance(x, str))
        df.loc[mask, profile_column] = df.loc[mask, profile_column].apply(
            lambda x: np.fromstring(x.strip('[]'), dtype=float, sep=', ')
        )
        df[profile_column] = df[profile_column].apply(lambda x: x.tolist())

    # Find the size of the first non-empty radial profile in TS_DATA
    first_valid_TS = TS_DATA["TF Decon Radial Profile Normalized"].dropna().iloc[0]
    size_TS = len(first_valid_TS) if isinstance(first_valid_TS, (np.ndarray, list)) else 0

    # Calculate averages and standard errors for TS_DATA
    if size_TS > 0:
        Y_TS = find_average_RP(TS_DATA, "TF Decon Radial Profile Normalized", size_TS)
        SEM_TS = find_sem(TS_DATA, Y_TS, "TF Decon Radial Profile Normalized", size_TS)
    else:
        print("No valid radial profiles found in TS_DATA")

    # Find the size of the first non-empty radial profile in RS_DATA
    first_valid_RS = RS_DATA["TF Decon RS Radial Profile Normalized"].dropna().iloc[0]
    size_RS = len(first_valid_RS) if isinstance(first_valid_RS, (np.ndarray, list)) else 0

    # Calculate averages and standard errors for RS_DATA
    if size_RS > 0:
        Y_RS = find_average_RP(RS_DATA, "TF Decon RS Radial Profile Normalized", size_RS)
        SEM_RS = find_sem(RS_DATA, Y_RS, "TF Decon RS Radial Profile Normalized", size_RS)
    else:
        print("No valid radial profiles found in RS_DATA")


    #common for all data sets
    x_values_max = ((window_crop-1)/2) * 0.1 #0.1 is the conversion from pixels to um 
    x_values_min = 0 * 0.1
    amount_of_values = len(Y_TS)
    step_size = x_values_max / (amount_of_values -1)
    x_values = np.arange(x_values_min, x_values_max+step_size, step_size).tolist()
    x_values = x_values ##all values in um

    #plotting
    plt.rcParams["figure.figsize"] = [10, 10]
    plt.rcParams["figure.autolayout"] = True
    sns.set(style="ticks", context="talk")
    if dark_background == True:
        plt.style.use("dark_background")
    else:
        plt.style.use("fivethirtyeight")
    fig = plt.figure()
    if dark_background ==False:
            fig.patch.set_facecolor('white')
    ax = fig.add_subplot(1, 1, 1)

    ax.plot(x_values, Y_TS, label = "transcription site", color = "deeppink")
    ax.plot(x_values, Y_RS, label = "random site", color = "mediumspringgreen")
    
    if save_edges == True:
        ax.plot(x_values, Y_EDGE, label = "edge sites", color = "darkorange")
        plt.fill_between(x_values, np.array(Y_EDGE) - np.array(SEM_EDGE), np.array(Y_EDGE) + np.array(SEM_EDGE), alpha=0.3, color = "darkorange")
        plt.fill_between(x_values, np.array(Y_TS) - np.array(SEM_TS), np.array(Y_TS) + np.array(SEM_TS), alpha=0.3, color = "deeppink")
        plt.fill_between(x_values, np.array(Y_RS) - np.array(SEM_RS), np.array(Y_RS) + np.array(SEM_RS), alpha=0.3, color = "mediumspringgreen")
        ax.legend(['transcription site', 'random site', 'edge sites'])
    else:
        plt.fill_between(x_values, np.array(Y_TS) - np.array(SEM_TS), np.array(Y_TS) + np.array(SEM_TS), alpha=0.3, color = "deeppink")
        plt.fill_between(x_values, np.array(Y_RS) - np.array(SEM_RS), np.array(Y_RS) + np.array(SEM_RS), alpha=0.3, color = "mediumspringgreen")
        ax.legend(['transcription site', 'random site'])

    ax.set_xlabel(f'Radius (um)')
    ax.set_ylabel(f'Relative Enrichment')
    ax.set_title(f'Transcription Factor Enrichment at Gene Loci ({sample_id})')
    plt.savefig(f'{analysis_dir}/Radial_Enrichment_Plot.svg')
    plt.savefig(f'{analysis_dir}/Radial_Enrichment_Plot.png')
    plt.show()
    plt.close()
    return

#generates a box plot of max enrichment from the data
def generate_box_plot(input_dir, sample_id, nuclearcycle, save_edges = False, dark_background = True, MCP_channel = 1, TF_channel = 0):
    sample_id = f"{sample_id}_{nuclearcycle}"
    analysis_dir  = f"{input_dir}/{sample_id}_analysis"
    os.makedirs(analysis_dir, exist_ok=True)

    TS_DATA = pd.read_csv(f"{input_dir}/{sample_id}_csvs/{sample_id}_all_transcription_sites.csv")
    RS_DATA = pd.read_csv(f"{input_dir}/{sample_id}_csvs/{sample_id}_all_random_sites.csv")
    
    if save_edges == True: 
        EDGE_DATA = pd.read_csv(f"{input_dir}/{sample_id}_csvs/{sample_id}_all_edge_sites.csv")

    #plotting
    plt.rcParams["figure.figsize"] = [10, 10]
    plt.rcParams["figure.autolayout"] = True
    sns.set(style="ticks", context="talk")
    if dark_background == True:
        plt.style.use("dark_background")
    else:
        plt.style.use("fivethirtyeight")
    fig = plt.figure()
    if dark_background == False:
            fig.patch.set_facecolor('white')
    ax = fig.add_subplot(1, 1, 1)

    if save_edges == True:
        ax.boxplot([TS_DATA['TF Max Enrichment'], 
                    TS_DATA['MCP Max Enrichment'], 
                    RS_DATA['TF Max Enrichment'], 
                    RS_DATA['MCP Max Enrichment'], 
                    EDGE_DATA['TF Max Enrichment'],
                    EDGE_DATA['MCP Max Enrichment']])
        ax.set_yticklabels(['TS_TF', 'TS_MCP','RS_TF', 'RS_MCP', 'ES_TF', 'ES_MCP'])
    else:
        ax.boxplot([TS_DATA['TF Max Enrichment'], 
                    TS_DATA['MCP Max Enrichment'], 
                    RS_DATA['TF Max Enrichment'], 
                    RS_DATA['MCP Max Enrichment']])
        ax.set_yticklabels(['TS_TF', 'TS_MCP','RS_TF', 'RS_MCP'])
    
    # ax.set_xlabel(f'Radius (um)')
    ax.set_ylabel(f'Relative Enrichment')
    ax.set_title(f'Max Enrichment Value ({sample_id})')
    # plt.savefig(f'{analysis_dir}/Radial_Enrichment_Plot.svg')
    # plt.savefig(f'{analysis_dir}/Radial_Enrichment_Plot.png')
    plt.show()
    # plt.close()
    return

@magic_factory(auto_call=False, call_button="Run DoG Selection")
def difference_of_gaussians_parameter_selection(viewer: "napari.Viewer",
                                                image: "napari.layers.Image", 
                                                sigma_low: float = 1.5, 
                                                sigma_high: float = 6.0):
    A = skimage.filters.gaussian(image.data, sigma_low) - skimage.filters.gaussian(image.data, sigma_high)
    viewer.add_image(A, name=f"DOG: low = {sigma_low}, high = {sigma_high}")
    return A

@magic_factory(auto_call=False,  # Disable auto-call
               threshold={"widget_type": "FloatSlider", "min": 95, "max": 100, "step": 0.001},
               call_button="Run Threshold Selection")  # Add a "Calculate" button
def threshold_parameter_selection(viewer: "napari.Viewer",
                                  image: "napari.layers.Image",
                                  D_MCP: "napari.layers.Image",
                                  threshold: float = 99,
                                  masks_provided: bool = False,
                                  M: "napari.layers.Labels" = None,
                                  max_size: int = 600) -> LabelsData:
    A = image.data > np.percentile(image.data, threshold)  # threshold for MS2 spots
    A = skimage.morphology.remove_small_objects(A, min_size=6)  # remove noise
    A = skimage.measure.label(A)  # label

    if masks_provided and M is not None:
        B = D_MCP.data * skimage.morphology.binary_erosion(M.data > 0, footprint=skimage.morphology.ball(3))  # remove non-nuclear things
        B = scipy.ndimage.median_filter(B, footprint=skimage.morphology.ball(1))  # maximum filter the entire normalized
        local_maxima = skimage.feature.peak_local_max(B, labels=M.data, num_peaks_per_label=1)
        A = A * (M.data > 0)
        A = filter_labels_by_seeds(A, local_maxima)
        A = recolor_based_on_overlap(A, M.data)
        A = remove_large_labels(A, max_size)
    
    viewer.add_labels(A, name=f"MS2 labels, % = {threshold}", opacity=1.0)
    return A

#used for results & interpretation
def see_results(output_dir, sample_id, nuclearcycle, window_crop, save_edges=True, dark_background = True):
    sample_id = f"{sample_id}_{nuclearcycle}"
    if save_edges== True:
        #open resulting plot 
        results = plt.imread(f"{output_dir}/{sample_id}_analysis/Radial_Enrichment_Plot.png")

        #open average tif files
        transcription_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_transcription_sites_Average_MCP.tif")
        random_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_random_sites_Average_MCP.tif")
        edge_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_edge_sites_Average_MCP.tif")

        transcription_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_transcription_sites_Average_TF.tif")
        random_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_random_sites_Average_TF.tif")
        edge_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_edge_sites_Average_TF.tif")

        # transcription_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_transcription_sites_Median_MCP.tif")
        # random_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_random_sites_Median_MCP.tif")
        # edge_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_edge_sites_Median_MCP.tif")

        # transcription_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_transcription_sites_Median_TF.tif")
        # random_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_random_sites_Median_TF.tif")
        # edge_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_edge_sites_Median_TF.tif")

        plt.rcParams["figure.figsize"] = [8, 20]
        plt.rcParams["figure.autolayout"] = True
        sns.set(style="ticks", context="talk")
        if dark_background == True:
            plt.style.use("dark_background")
        else:
            plt.style.use("fivethirtyeight")

        plt.close('all')
        fig = plt.figure()
        if dark_background ==False:
            fig.patch.set_facecolor('white')

        ax7 = plt.subplot2grid((5,2), (0,0), colspan=2, rowspan=2)
        ax1 = plt.subplot2grid((5,2), (2,0))
        ax2 = plt.subplot2grid((5,2), (2,1))
        ax3 = plt.subplot2grid((5,2), (3,0))
        ax4 = plt.subplot2grid((5,2), (3,1))
        ax5 = plt.subplot2grid((5,2), (4,0))
        ax6 = plt.subplot2grid((5,2), (4,1))

        ax7.imshow(results)
        ax7.axis('off')

        # min_mcp = np.min([np.min(random_sites_image_mcp), np.min(transcription_sites_image_mcp), np.min(edge_sites_image_mcp)])
        # min_tf = np.min([np.min(random_sites_image_tf), np.min(transcription_sites_image_tf), np.min(edge_sites_image_tf)])
        # max_mcp = np.max([np.max(random_sites_image_mcp), np.max(transcription_sites_image_mcp), np.max(edge_sites_image_mcp)])
        # max_tf = np.max([np.max(random_sites_image_tf), np.max(transcription_sites_image_tf), np.max(edge_sites_image_tf)])
        min_mcp = np.min([np.min(random_sites_image_mcp), np.min(transcription_sites_image_mcp)])
        min_tf = np.min([np.min(random_sites_image_tf), np.min(transcription_sites_image_tf)])
        max_mcp = np.max([np.max(random_sites_image_mcp), np.max(transcription_sites_image_mcp)])
        max_tf = np.max([np.max(random_sites_image_tf), np.max(transcription_sites_image_tf)])          

        ax1.imshow(np.reshape(transcription_sites_image_mcp, (window_crop,window_crop)), interpolation="none", cmap = 'magma', vmin = min_mcp, vmax = max_mcp)
        ax1.set_title('Transcription Site MCP', fontsize=18)
        ax1.axis('off')
        ax2.imshow(np.reshape(transcription_sites_image_tf, (window_crop,window_crop)), interpolation="none", cmap = 'viridis', vmin = min_tf, vmax = max_tf)
        ax2.set_title('Transcription Site TF', fontsize=18)
        ax2.axis('off')

        ax3.imshow(np.reshape(random_sites_image_mcp, (window_crop,window_crop)), interpolation="none", cmap = 'magma', vmin = min_mcp, vmax = max_mcp)
        ax3.set_title('Random Site MCP', fontsize=18)
        ax3.axis('off')
        ax4.imshow(np.reshape(random_sites_image_tf, (window_crop,window_crop)), interpolation="none", cmap = 'viridis', vmin = min_tf, vmax = max_tf)
        ax4.set_title('Random Site TF', fontsize=18)
        ax4.axis('off')

        ax5.imshow(np.reshape(edge_sites_image_mcp, (window_crop,window_crop)), interpolation="none", cmap = 'magma', vmin = min_mcp, vmax = max_mcp)
        ax5.set_title('Edge Site MCP', fontsize=18)
        ax5.axis('off')
        ax6.imshow(np.reshape(edge_sites_image_tf, (window_crop,window_crop)), interpolation="none", cmap = 'viridis', vmin = min_tf, vmax = max_tf)
        ax6.set_title('Edge Site TF', fontsize=18)
        ax6.axis('off')

        plt.tight_layout()
    else:
        #open resulting plot 
        results = plt.imread(f"{output_dir}/{sample_id}_analysis/Radial_Enrichment_Plot.png")

        #open average tif files
        transcription_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_transcription_sites_Average_MCP.tif")
        random_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_random_sites_Average_MCP.tif")
        # edge_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_edge_sites_Average_MCP.tif")

        transcription_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_transcription_sites_Average_TF.tif")
        random_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_random_sites_Average_TF.tif")
        # edge_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_edge_sites_Average_TF.tif")

        # transcription_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_transcription_sites_Median_MCP.tif")
        # random_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_random_sites_Median_MCP.tif")
        # # edge_sites_image_mcp = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_edge_sites_Average_MCP.tif")

        # transcription_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_transcription_sites_Median_TF.tif")
        # random_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_random_sites_Median_TF.tif")
        # # edge_sites_image_tf = tifffile.imread(f"{output_dir}/{sample_id}_analysis/{sample_id}_edge_sites_Average_TF.tif")

        plt.rcParams["figure.figsize"] = [8, 20]
        plt.rcParams["figure.autolayout"] = True
        sns.set(style="ticks", context="talk")
        if dark_background == True:
            plt.style.use("dark_background")
        else:
            plt.style.use("fivethirtyeight")

        plt.close('all')
        fig = plt.figure()

        if dark_background ==False:
            fig.patch.set_facecolor('white')

        ax7 = plt.subplot2grid((4,2), (0,0), colspan=2, rowspan=2)
        ax1 = plt.subplot2grid((4,2), (2,0))
        ax2 = plt.subplot2grid((4,2), (2,1))
        ax3 = plt.subplot2grid((4,2), (3,0))
        ax4 = plt.subplot2grid((4,2), (3,1))
        # ax5 = plt.subplot2grid((5,2), (4,0))
        # ax6 = plt.subplot2grid((5,2), (4,1))

        ax7.imshow(results)
        ax7.axis('off')

        min_mcp = np.min([np.min(random_sites_image_mcp), np.min(transcription_sites_image_mcp)])
        min_tf = np.min([np.min(random_sites_image_tf), np.min(transcription_sites_image_tf)])
        max_mcp = np.max([np.max(random_sites_image_mcp), np.max(transcription_sites_image_mcp)])
        max_tf = np.max([np.max(random_sites_image_tf), np.max(transcription_sites_image_tf)])

        ax1.imshow(np.reshape(transcription_sites_image_mcp, (window_crop,window_crop)), interpolation="none", cmap = 'magma', vmin = min_mcp, vmax = max_mcp)
        ax1.set_title('Transcription Site MCP', fontsize=18)
        ax1.axis('off')
        ax2.imshow(np.reshape(transcription_sites_image_tf, (window_crop,window_crop)), interpolation="none", cmap = 'viridis', vmin = min_tf, vmax = max_tf)
        ax2.set_title('Transcription Site TF', fontsize=18)
        ax2.axis('off')

        ax3.imshow(np.reshape(random_sites_image_mcp, (window_crop,window_crop)), interpolation="none", cmap = 'magma', vmin = min_mcp, vmax = max_mcp)
        ax3.set_title('Random Site MCP', fontsize=18)
        ax3.axis('off')
        ax4.imshow(np.reshape(random_sites_image_tf, (window_crop,window_crop)), interpolation="none", cmap = 'viridis', vmin = min_tf, vmax = max_tf)
        ax4.set_title('Random Site TF', fontsize=18)
        ax4.axis('off')


        plt.tight_layout()
    return 

#initializes first view
def initialize(image_folder, type = "raw"):
    if type == "nuclear mask":
        IMG = MOSAIC.MOSAICImage(image_folder, dualchannel=False, cam='CamA')
    else:
        IMG = MOSAIC.MOSAICImage(image_folder, dualchannel=True, cam=['CamA', 'CamB'])
    return IMG

#opens one timepoint image in napari
def open_image(IMG, channel, time, Z_Bounds, Viewer, type):
    if Z_Bounds == [0,0]:
        Z_Bounds = None
    else:
        Z_Bounds = [int(Z_Bounds[0]), int(Z_Bounds[1])]
    if type == "raw":
        if Z_Bounds == None:
            I = IMG.open_inx(channel, time) #this opens one specific timepoint
        else:
            I = IMG.open_inx(channel, time)[Z_Bounds[0]:Z_Bounds[1],: ,:]
    else:
        I = IMG.open_inx(channel, time)
    if type == "nuclear mask":
        if Z_Bounds is not None:
            I = I[Z_Bounds[0]:Z_Bounds[1],: ,:]
        Viewer.add_labels(I, name = f"timepoint {time} of {type} image", opacity = 0.4) #this initializes your napari viewer with your first timepoint
    else:
        Viewer.add_image(I, name = f"timepoint {time} of {type} image") #this initializes your napari viewer with your first timepoint
    return I

#opens mask with all random spots generated
def check_nucleus_image(t, DIMG, spot_mask,  raw_mcp_image, decon_mcp_image, nuclear_point, cytoplasmic_point, viewer, adjustment_factor = 1.0, MCP_CHANNEL = 1, window_crop  = 21, masks_provided = False, nuc_mask = None, save_edges = False):
    #copy incase we need to make changes
    A = np.copy(spot_mask)
    M = np.copy(nuc_mask)
    I_MCP = np.copy(raw_mcp_image)
    D_MCP = np.copy(decon_mcp_image)

    #intiailize some key dataframes needed for later analysis
    RS_points = [] #make empty array to hold random points
    RS_points_2 = [] #make empty array to hold random points
    TS_points = [] #make empty array to hold transcription site points
    ES_points = [] #make empty array to hold edge site points

    if masks_provided == False: 
        #Validate Nuclear Localization -> needed if no nuclear masks are provided 
        #find the nuclear background and the cytoplasmic background levels to better assess nuclear localization
        #to find the avergae take a X and Y line profile through the two point and then take the average to help remove noise thru the points
        NI = DIMG.open_inx(MCP_CHANNEL, nuclear_point[0])[:,:,:] 
        CI = DIMG.open_inx(MCP_CHANNEL, cytoplasmic_point[0])[:,:,:] 
        nuc_background  = get_lower_quartile_mean(NI[int(nuclear_point[1]), int(nuclear_point[2]-((window_crop-1)/2)):int(nuclear_point[2]+((window_crop-1)/2))+1,  int(nuclear_point[3]-((window_crop-1)/2)):int(nuclear_point[3]+((window_crop-1)/2))+1])
        cyto_background  = get_upper_quartile_mean(CI[int(cytoplasmic_point[1]), int(cytoplasmic_point[2]-((window_crop-1)/2)):int(cytoplasmic_point[2]+((window_crop-1)/2))+1,  int(cytoplasmic_point[3]-((window_crop-1)/2)):int(cytoplasmic_point[3]+((window_crop-1)/2))+1])
        ratio_nc = 1 - (nuc_background/cyto_background)
        nuc_threshold = nuc_background*(1+ratio_nc)*adjustment_factor #anything above this value it cytoplasmic, under is nuclear      
    
    else: #find MS2 spots within the nucleus 
        B = D_MCP * skimage.morphology.binary_erosion(M>0, footprint = skimage.morphology.ball(3)) #remove non-nuclear things
        B = scipy.ndimage.median_filter(B, footprint = skimage.morphology.ball(1))  #maximum filter the entire normalized
        local_maxima = skimage.feature.peak_local_max(B, labels = M, num_peaks_per_label = 1) #find local maxima
        A = filter_labels_by_seeds(A, local_maxima) 
        A = recolor_based_on_overlap(A, M)

    ##Summarize Points
    regions = skimage.measure.regionprops(A, intensity_image=D_MCP) #identify properties of the raw MS2 spots in label matrix
    new_points = pd.DataFrame({
        'Label':[r.label for r in regions], #this is also the same as the label for the nucleus
        'T':[t for _ in regions],  #time tag so that once the nuclei are tracked we will be able to fully get the tracking
        'Xi_MS2':[np.round(r.weighted_centroid[2]) for r in regions],
        'Yi_MS2':[np.round(r.weighted_centroid[1]) for r in regions],
        'Zi_MS2':[np.round(r.weighted_centroid[0]) for r in regions],
        'Nuclear?':[False for _ in regions],
        'Edge?':[False for _ in regions]
    })

    # create dataframe containing all nuclear points 
    for inx in range(len(new_points)): #for every label in matrix
        # print(f'Index = {inx} / {max_label_value}')
        ##nuclear validation
        p = new_points.iloc[inx] #for one label
        TS = [int(p['Zi_MS2']), int(p['Yi_MS2']), int(p['Xi_MS2'])]
        TSi = [int(TS[0])-1, int(TS[1]-((window_crop-1)/2)), int(TS[2]-((window_crop-1)/2))]
        TSf = [int(TS[0])+1, int(TS[1]+((window_crop-1)/2))+1, int(TS[2]+((window_crop-1)/2))+1]

        #if window crop is beyond image boundaries, set as nonnuclear and discard the point 
        if TSi[2]-1 < 0 or TSf[2]+1 > (DIMG.shape[4]-1) or TSi[1]-1 < 0 or TSf[1]+1 > (DIMG.shape[3]-1) or TSi[0]-1 < 0 or TSf[0]+1 > (DIMG.shape[2]-1):
            new_points.at[inx, 'Nuclear?'] = False
            new_points.at[inx, 'Edge?'] = False

        #otherwise assess nuclear localization
        else:
            if masks_provided == False:
                x = TS[2]
                y = TS[1]
                for theta in range(0, 180, 30):
                    xi = int(x + ((window_crop-1)*0.5*math.cos(math.radians(theta))))
                    yi = int(y + ((window_crop-1)*0.5*math.sin(math.radians(theta))))
                    xf = int(x - ((window_crop-1)*0.5*math.cos(math.radians(theta))))
                    yf = int(y - ((window_crop-1)*0.5*math.sin(math.radians(theta))))
                    RS_line_profile = np.asarray(skimage.measure.profile_line(I_MCP[int(TS[0]),:,:], [yi, xi], [yf, xf], linewidth=2))
                    ratio = (np.median(RS_line_profile[:int((window_crop-1)/5)])/np.median(RS_line_profile[int(window_crop-((window_crop-1)/5)):]))
                    if (ratio > (1 + ratio_nc)) or (ratio < (1 - ratio_nc)): #if signal:noise less than desired ratio threshold then discard ##use this for finer control and specificity
                        new_points.at[inx, 'Edge?'] = True
                    elif (get_lower_quartile_mean(RS_line_profile[:int((window_crop-1)/5)]) >= nuc_threshold):
                        new_points.at[inx, 'Edge?'] = True
                    elif (get_lower_quartile_mean(RS_line_profile[int(window_crop-((window_crop-1)/5)):]) >= nuc_threshold):
                        new_points.at[inx, 'Edge?'] = True
                    else:
                        #now add adjusted center of MS2 point and standard deviation to the data frame
                        new_points.at[inx, 'Nuclear?'] = True
                if new_points.at[inx, 'Edge?'] == True:
                    new_points.at[inx, 'Nuclear?'] = False
            elif masks_provided == True:
                new_points.at[inx, 'Nuclear?'] = True
                #now check to assess edge status 
                new_points.at[inx, 'Edge?'] = not is_within_object_square(M, TS, size = window_crop)

        #IF IT IS NUCLEAR 
        if (new_points.at[inx, 'Nuclear?'] == True) and (new_points.at[inx, 'Edge?'] == False): #adjust RS criteria to include checking for either mask images or non-mask images
            if masks_provided == True:
                RS_points_result = find_random_nuclear_spots(DIMG, I_MCP, M, p, window_crop, ratio_nc=0, nuc_threshold = 0, masks_provided=masks_provided)
            else: 
                RS_points_result = find_random_nuclear_spots(DIMG, I_MCP, M, p, window_crop, ratio_nc, nuc_threshold, masks_provided=masks_provided)
            RS_counter = len(RS_points_result)
            if RS_counter == 2 or RS_counter == 3:
                for r in RS_points_result:
                    RS_points.append([r[2], r[3], r[4]]) #ZYX of the random points 
                TS_points.append([p['Zi_MS2'], p['Yi_MS2'], p['Xi_MS2']])

        elif (new_points.at[inx, 'Edge?'] == True) and (save_edges == True):
            #edge random sites
            if masks_provided == True:
                RS_points_result = find_random_nuclear_spots(DIMG, I_MCP, M, p, window_crop, ratio_nc = 0, nuc_threshold = 0, masks_provided=masks_provided)
            else: 
                RS_points_result = find_random_nuclear_spots(DIMG, I_MCP, M, p, window_crop, ratio_nc, nuc_threshold, masks_provided=masks_provided)
            RS_counter = len(RS_points_result)
            if RS_counter == 2 or RS_counter == 3:
                for r in RS_points_result:
                    RS_points_2.append([r[2], r[3], r[4]]) #ZYX of the random points 
                ES_points.append([p['Zi_MS2'], p['Yi_MS2'], p['Xi_MS2']])
    
    #all random spots saved here: RS_points
    viewer.add_points(RS_points, name = f"Transcription Random Spots", face_color = 'green', size = 5, opacity = 0.5)
    #all edge randoms saved here: RS_points_2
    viewer.add_points(RS_points_2, name = f"Edge Random Spots", face_color = 'pink', size = 5, opacity = 0.5)
    #all TS saved here: TS_points
    viewer.add_points(TS_points, name = f"Transcription Sites", face_color = 'green', size = 5, opacity = 0.5)
    #all ES saved here: ES_points
    viewer.add_points(ES_points, name = f"Edge Sites", face_color = 'pink', size = 5, opacity = 0.5)
    return 

# Define a function to replace the prefix of a given path
def replace_path_prefix(path, old_prefix="/Volumes", new_prefix="/mnt/isilon"):
    if path.startswith(old_prefix):
        # Replace the old prefix with the new prefix
        return path.replace(old_prefix, new_prefix)
    else:
        # Return the original path if the old prefix is not found
        return path
    
def read_CSV(dataset_parameters, row_number):
    params = pd.read_csv(dataset_parameters).iloc[row_number]
    os.makedirs(params['output_path'], exist_ok=True) #this creates a directory in case the output folder does not exist
    return params 

def initialize_viewer():
    # Initialize the napari viewer
    viewer = napari.Viewer()
    # Instantiate the widget
    dog_widget = difference_of_gaussians_parameter_selection()
    # Create the widget using the decorated function
    threshold_parameter_widget = threshold_parameter_selection()
    
    # Add the instantiated widget to the viewer
    viewer.window.add_dock_widget(dog_widget, area='left')

    # Add the widget to the viewer
    viewer.window.add_dock_widget(threshold_parameter_widget, area='left')
    # print("HI")
    return viewer

def generate_spotmask(image, sigma_low, sigma_high, Viewer, D_MCP, threshold, masks_provided = False, M = None, max_size = 600):
    A = skimage.filters.gaussian(image, sigma_low) - skimage.filters.gaussian(image, sigma_high)
    ##Segment MS2 Spots
    A = A > np.percentile(A, threshold) #threshold for MS2 spots
    A = skimage.morphology.remove_small_objects(A, min_size=6) #remove noise
    A = skimage.measure.label(A) #label

    if masks_provided == True and M is not None: 
        B = D_MCP * skimage.morphology.binary_erosion(M>0, footprint = skimage.morphology.ball(3)) #remove non-nuclear things
        B = scipy.ndimage.median_filter(B, footprint = skimage.morphology.ball(1))  #maximum filter the entire normalized
        local_maxima = skimage.feature.peak_local_max(B, labels = M, num_peaks_per_label = 1)
        A = A * (M>0)
        A = filter_labels_by_seeds(A, local_maxima)
        A = recolor_based_on_overlap(A, M)
        A = remove_large_labels(A, max_size)

    # viewer.add_labels(A, name=f"MS2 labels, % = {threshold}", opacity=1.0)
    return A

###Updated code for multiprocessing

def f(t, 
        image_folder, 
        decon_folder,
        mask_folder, 
        sample_id,
        nuclearcycle,
        output_dir,
        sigma_high,
        sigma_low,
        nuclear_point,
        cytoplasmic_point,
        threshold,
        adjustment_factor,
        TF_CHANNEL,
        MCP_CHANNEL, 
        window_crop,
        Z_Bounds,
        save_edges,
        masks_provided):
    
    #Initialize the input images
    IMG = MOSAIC.MOSAICImage(image_folder, dualchannel=True, cam=['CamA', 'CamB'])
    DIMG = MOSAIC.MOSAICImage(decon_folder, dualchannel=True, cam=['CamA', 'CamB'])
    if masks_provided == True:
        MIMG = MOSAIC.MOSAICImage(mask_folder, dualchannel=False, cam='CamA')

    return spot_segmentation_and_crop(t, 
                            IMG = IMG, 
                            DIMG = DIMG,
                            MIMG = MIMG, 
                            sample_id = sample_id,
                            nuclearcycle = nuclearcycle,
                            input_dir = image_folder,
                            output_dir = output_dir,
                            sigma_high = sigma_high,
                            sigma_low = sigma_low,
                            nuclear_point = nuclear_point,
                            cytoplasmic_point = cytoplasmic_point,
                            thresh_value = threshold,
                            adjustment_factor = adjustment_factor,
                            TF_CHANNEL = TF_CHANNEL,
                            MCP_CHANNEL = MCP_CHANNEL, 
                            window_crop = window_crop,
                            Z_Bounds = Z_Bounds,
                            save_edges = save_edges,
                            masks_provided = masks_provided)

def process_row(params):
    # Unpack all the parameters from the row
    sample_id = params["sample_id"]
    image_folder = replace_path_prefix(params["Idir"])
    decon_folder = f"{image_folder}/GPUdecon"
    mask_folder = f"{image_folder}/Tracked_Masks"
    output_holder = replace_path_prefix(params["output_path"])
    output_dir = f"{output_holder}/{sample_id}_pyEnRICH"
    # Convert the nuclear cycle information into a list of tuples
    nuclearcycle = [
        ('NC12', [params['NC12_start'], params['NC12_end']]),
        ('NC13', [params['NC13_start'], params['NC13_end']]),
        ('NC14', [params['NC14_start'], params['NC14_end']])
    ]
    MCP_CHANNEL = params['TF_CHANNEL']
    TF_CHANNEL = params['MCP_CHANNEL']
    window_crop = params['window_crop']
    sigma_high = params['sigma_high']
    sigma_low = params['sigma_low']
    threshold = params['threshold']
    adjustment_factor = params['adjustment_factor']
    nuclear_point = params['nuclear_point']
    cytoplasmic_point = params['cytoplasmic_point']
    if nuclear_point == "None":
        nuclear_point = None
        cytoplasmic_point = None
    else:
        nuclear_point = ast.literal_eval(nuclear_point)
        cytoplasmic_point = ast.literal_eval(cytoplasmic_point)
    Z_Bounds = [params['Z_Bounds_start'], params['Z_Bounds_end']]
    if Z_Bounds[0] == Z_Bounds[1]:
        Z_Bounds = None
    save_edges = params['save_edges']
    masks_provided = params['masks_provided']

    # Run the processing for each nuclear cycle
    for nc, nc_t in nuclearcycle:
        NC_start = nc_t[0]
        NC_end   = nc_t[1]
        if NC_start == NC_end:
            continue
        else:
            with Pool(processes=params['number_of_parallel_processes']) as pool:
                args = [(t, image_folder, decon_folder, mask_folder, sample_id,nuclearcycle,output_dir,sigma_high, sigma_low,nuclear_point,cytoplasmic_point,threshold,adjustment_factor,TF_CHANNEL, MCP_CHANNEL,  window_crop,Z_Bounds,save_edges,masks_provided) 
                        for t in range(NC_start, NC_end)]  # Adjust range as necessary
                pool.starmap(f, args)

        generate_figures(image_folder, 
                         output_dir, 
                         sample_id, 
                         nc, 
                         window_crop, 
                         save_edges = True, 
                         dark_background = True, 
                         MCP_channel = 1, 
                         TF_channel = 0)

def main():
    # Read the parameters from the CSV file
    params_df = pd.read_csv('/mnt/isilon/mirlab/S/Analysis/dataset_parameters2.csv')

    # Iterate over each row in the DataFrame and process it
    for _, row in params_df.iterrows():
        if row['pyEnRICH'] == 1:
            process_row(row)

if __name__ == '__main__':
    main()
