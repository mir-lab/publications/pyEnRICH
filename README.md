# pyEnRICH
a PYthon framework for Enriched Radial Intensity Calculations for Hubs

## Install

1. Create a `conda` environment for `pyEnRICH`. (If you don't already have it, you'll need `conda`: https://docs.conda.io/en/latest/miniconda.html.) Navigate to the top-level `pyEnRICH` directory and run 

```
    conda env create -f pyEnRICH_env.yml
```

2. Switch to the `pyEnRICH_env` environment:

```
    conda activate pyEnRICH_env
```

3. Finally, navigate to the notebook `pyEnRICH/pyEnRICH/pyEnRICH_tutorial.ipynb` to learn how to use pyEnRICH. 

## Prior Processing
This pipeline requires you to deconvolve your data prior to processing. We use cudadecon which is GPU accelerated 3D image deconvolution using CUDA (requires Nvidia GPU) developed in the Betzig lab at Janelia by Lin Shao and Dan Milkie. The main code for this can be found here: https://github.com/scopetools/cudaDecon



